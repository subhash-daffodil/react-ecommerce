var React = require('react');
var TypeaheadOption = require('./options');
var classNames = require('classnames');
import config from "../common/config.jsx";

/**
 * Container for the options rendered as part of the autocompletion process
 * of the typeahead
 */
var TypeaheadSelector = React.createClass({
  propTypes: {
    options: React.PropTypes.array,
    allowCustomValues: React.PropTypes.number,
    customClasses: React.PropTypes.object,
    customValue: React.PropTypes.string,
    selectionIndex: React.PropTypes.number,
    onOptionSelected: React.PropTypes.func,
    displayOption: React.PropTypes.func.isRequired,
    defaultClassNames: React.PropTypes.bool
  },

  getDefaultProps: function() {
    return {
      selectionIndex: null,
      customClasses: {},
      allowCustomValues: 0,
      customValue: null,
      onOptionSelected: function(option) { },
      defaultClassNames: true
    };
  },

  render: function() {
    // Don't render if there are no options to display
    if (!this.props.options.length && this.props.allowCustomValues <= 0) {
      return false;
    }

    var classes = {
      "typeahead-selector": this.props.defaultClassNames
    };
    classes[this.props.customClasses.results] = this.props.customClasses.results;
    var classList = classNames(classes);

    // CustomValue should be added to top of results list with different class name
    var customValue = null;
    var customValueOffset = 0;
    if (this.props.customValue !== null) {
      customValueOffset++;
      customValue = (
        <TypeaheadOption ref={this.props.customValue} key={this.props.customValue}
          hover={this.props.selectionIndex === 0}
          customClasses={this.props.customClasses}
          customValue={this.props.customValue}
          onClick={this._onClick.bind(this, this.props.customValue)}>
          { this.props.customValue }
        </TypeaheadOption>
      );
    }
    var host =  config.baseApiUrl;
    var results = this.props.options.map(function(result, i) {
      // var displayString = this.props.displayOption(result, i);
      var displayResult = JSON.parse(result);

      var uniqueKey = "displayString" + '_' + i;
      return (
        <TypeaheadOption ref={uniqueKey} key={uniqueKey}
          hover={this.props.selectionIndex === i + customValueOffset}
          customClasses={this.props.customClasses}
          onClick={this._onClick.bind(this, result)}>
          <div className="col-xs-12 none" style={{"paddingBottom": "4px"}}>
              <div className="col-md-1  none">  
                <img src={host + displayResult.trending_image} />
              </div>
              <div className="col-md-9 col-sm-8 col-xs-8">
                <div className="col-xs-7 none">
                   <div className="col-xs-12 " >{displayResult.name.length > 50 ? displayResult.name.substring(0,50)+"..." : displayResult.name}</div>
                   <div className="col-xs-12 " >{displayResult.description.length > 50 ? displayResult.description.substring(0,50)+"..." : displayResult.description}</div></div>
                                
              </div>
            </div>
        </TypeaheadOption>
      );
    }, this);

    return (
      <ul className={classList} style={{"padding":"2px"}}>
        { customValue }
        { results }
      </ul>
    );
  },

  _onClick: function(result, event) {
    return this.props.onOptionSelected(result, event);
  }

});

module.exports = TypeaheadSelector;