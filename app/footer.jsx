import React, { Component } from 'react';
import { Link } from 'react-router';
import config from "./common/config.jsx"

export default class Footer extends Component{
  render() {
    return (
      <div>
         <div className="panel-footer col-xs-12 text-center padding_lg" id="footer">
	      <ul className="footer none">
	        <li><a className="text_3" href="#">{"@2016 " + config.store_Name }</a></li>
	        <li><Link to="about" className="text_3" href="#">About Us</Link></li>
	        <li><Link to="contact" className="text_3" href="#">Contact Us</Link></li>
	        <li><a className="text_3" href="#">Terms</a></li>
	        <li><a className="text_3" href="#">Privacy</a></li>
	        <li><a className="text_3" href="#">Copyright</a></li>
	      </ul>
		</div>
      </div>
    )
  }
}