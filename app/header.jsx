import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import { setUser } from './actions/user.jsx';
import _ from 'underscore';
import config from "./common/config.jsx";
import API from "./common/api.jsx";
import NavMenu from './components/navMenu.jsx';
import CategoryLink from './components/categoryLink.jsx';
import DialogBox from './components/dialogBox.jsx';
import HeaderCart from './components/headerCart.jsx';
import Logout from './components/logout.jsx';

let baseAPIUrl = config.baseApiUrl;
var TypeaheadSelector = require('./Typeahead/selector.js');
var Typeahead = require('./Typeahead/typeahead.js');

class Header extends Component{
  constructor(props) {
    super(props);
    this.searchData = _.debounce(this.searchData,"500");
    this.state = {
      typeHeadValue: "",
      showMenu : false,
      showCart : false,
      showUser : false,
      userName: null,
      cart_data: {}
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(setUser(null));
    this.userLogged();
  }

  componentWillReceiveProps(props) {
    this.setCart(props);
  }

  setCart(props) {
    const { state, cartReducer } = (props && Object.keys(props).length) ? props : this.props;
    let cart = (cartReducer.cart && Object.keys(cartReducer.cart).length) ? cartReducer.cart.cart_data : {};
    this.setState({cart_data: cart});
  }

  openMenu() {
    this.setState({
      showMenu : true
    })
  }

  closeMenu() {
    this.setState({
      showMenu : false
    })
  }

  userLogged() {
    var that = this;
    let { user } = this.props;
    if(user && typeof(user) === "string") {
      user = JSON.parse(user);
    }
    if(user && Object.keys(user).length) {
      var userName;
      if(user.username) {
        userName =  user.username;
        userName = userName.indexOf("@") == -1 ? userName : userName.substring(0, userName.indexOf("@"));
      } else {
        userName =  user.first_name + " " + user.last_name;
      }
      that.setState({
        userName : userName
      });
      that.props.onSwitchUser(user);
    } else {
      that.setState({
        userName : null
      });
    }
    that.props.onFetchCart();
  }

  searchData(query) {
    var self = this;

    if(query && query.length > 0) {
      self.setState({
        query : query
      })
      API.get(baseAPIUrl + "search?query="+query+"&limit=6&skip=0", null, function(err,result,more) {
        if(err || result === 'error' || more && more.responseText.indexOf('error') > -1) {
          console.log("<-----get product for search ------>");
          console.log("Error:", err);
          console.log("Result:", result);
          console.log("Details:", more);
          console.log("<------get product for search ------>");
        } else {
          result = _.map(result,function(obj){
            return JSON.stringify(obj);
          }) 
          // result[1] = result[0];
          self.setState({
            query : query,
            searchResult : result
          })
        }
      });
    }

  }

  displayOption(option) {
    option = JSON.parse(option)
    return option.name
  }

  optionSelected(selection) {
     if (selection) {
       var product = JSON.parse(selection);
       this.setState({
          query : null,
          searchResult : []
        },function() {
          this.props.history.push("/product/"+product.product_code);
        });
     } else {
      console.log('Error: header.js. Line: 162')
      // if(this.state.searchResult && this.state.searchResult.length > 0)
      //   this.props.history.push("/search/"+this.state.query); 
     }
  }

  onEnterPress (event) {
    var query =  this.state.query;
    this.setState({
      query : null,
      searchResult : []
    },function() {
      this.props.history.push("/search/" + query);
    });
  }
  
  render() {

    return (
      <div className="none col-xs-12 bg_1 padding-top-5" id="header">
        {this.state.showMenu ? (
          <NavMenu storeCategory = {this.props.storeCategory} onOpenMenu={(event) => {this.openMenu(event)}} onCloseMenu={(event) => {this.closeMenu(event)}} />
        ) : null}
        <div className="col-md-3 col-sm-3 col-xs-5 f4 logo none">
          <span className="col-md-2 col-sm-2 category-button glyphicon glyphicon-align-justify" onMouseEnter={this.openMenu.bind(this)} onMouseLeave={(event) => {this.closeMenu(event)}}></span>
          <Link to={"/"}><span className="text_3 ">{this.props.storeData.name}</span></Link>
        </div>
        <div className="col-md-3 col-sm-4 col-xs-7 pull-right login_bar margin-top-3">
          <HeaderCart cart_data={this.state.cart_data} onOpenCloseDialog={this.props.onOpenCloseDialog} />
          <Logout onOpenCloseDialog={this.props.onOpenCloseDialog} userName={this.state.userName} {...this.props} />
        </div>
        <div className="search col-md-6 col-sm-5 col-xs-6 padding_sm phone tab search_all" style={{height: '48px'}}>
          <Typeahead value={this.state.typeHeadValue} options={this.state.searchResult} maxVisible={10} customListComponent= {TypeaheadSelector} selectionIndex={null} displayOption={this.displayOption} onChange={(event) => {this.searchData(event.target.value)}}  onOptionSelected = {(val) => {this.optionSelected(val)}} onEnterPress={this.onEnterPress.bind(this)} />                  
          <button className="btn_3"><i className="glyphicon glyphicon-search icon f10"></i></button>
        </div>
        <DialogBox {...this.props} onOpenCloseDialog={this.props.onOpenCloseDialog} onUserLogged={(event) => {this.userLogged(event)}}/>
        <CategoryLink storeCategory = {this.props.storeCategory}/>
      </div>
    )
  }
}


class Attributes extends Component { 

  render() {  
    var attributes = this.props.data.map(function(attribute, index){      
      return (
        <div key={"attribute_" + index} className="col-xs-12 none f11" >
          <b>{attribute.name}</b> : {attribute.value}
        </div>   
      );        
    });
    return (
      <div> 
        {attributes} 
      </div>
    );
  }
}

class Options extends Component { 

  render() {  
    var listOptions = this.props.options.map(function(product){   
      var product =  JSON.parse(product);  
      return (
        <li>{pro.name}</li>         
      )        
    })
    return (
      <ul> 
        {listOptions}
      </ul>
    )
  }
}

const mapStateToProps = (state) => {
  let user = (state.userReducer && Object.keys(state.userReducer).length) ? state.userReducer.authUser : {};
  return {state: state.homeReducer, user: user, cartReducer: state.cartReducer}
}

Header = connect(mapStateToProps, null)(Header)
export default Header;