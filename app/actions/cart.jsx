import Redux from 'redux';

export const SAVE_CART = 'SAVE_CART'
export const ADD_CART = 'ADD_CART'
export const EVALUATE_CART = 'EVALUATE_CART'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const REVIEW_CART = 'REVIEW_CART'
export const UPDATE_CART = 'UPDATE_CART'

import API from '../common/api.jsx';
import config from "../common/config.jsx";
let baseAPIUrl = config.baseApiUrl;

export function saveCart(cart) {
	return {type: SAVE_CART, cart}
}

export function getCartAPI() {
  	return (dispatch) => {
	    API.get(baseAPIUrl + "cart/item/",null, (err, data, more) => {
	    	if(data) {
	    		let cart = updateCartData(data);
	    		dispatch(saveCart(cart));
	    	}
	    });
  	}
}

export function removeItemAPI(itemData) {
	return (dispatch) => {
	    API.put(baseAPIUrl + "cart/itemremove/"+ itemData.id, itemData, (err, data, more) => {
	    	if(data) {
	    		let cart = updateCartData(data);
	    		dispatch(saveCart(cart));
	    	}
	    });
  	}
}

export function qtyChangedAPI(item) {
	return (dispatch) => {
	    API.put(baseAPIUrl + "cart/item/",item, (err, data, more) => {
	    	if(data) {
	    		let cart = updateCartData(data);
	    		dispatch(saveCart(cart));
	    	}
	    });
  	}
}

export function updateCartAPI(item) {
	return (dispatch) => {
	    API.post(baseAPIUrl + "cart/item", item, (err, data, more) => {
	    	if(data) {
	    		let cart = updateCartData(data);
	    		dispatch(saveCart(cart));
	    	}
	    });
  	}
}

function updateCartData(cart_data) {
	var data = {};   
  	data.cart_data = cart_data;
  	data.subtotal = 0;
  	data.shippingCost = 0;
  	data.totalExtendedPrice = 0;
  	data.totalDiscount = 0;
  	data.totalExtendedPrice = 0;
  	data.totalQuantityOrder = 0;
  	data.total_coupon_discount = 0;
  	if(data.cart_data && data.cart_data.items){
    	data.vat = cart_data.vat_total;
    	data.sales_tax = cart_data.sales_tax_total;
        data.shippingCost = cart_data.shipping_cost_total;
        for (var i = 0; i < data.cart_data.items.length; i++) {
          	for (var j = 0; j < data.cart_data.items[i].store_items.length; j++) {
	            data.totalQuantityOrder = data.totalQuantityOrder + data.cart_data.items[i].store_items[j].quantity_ordered;
	            data.cart_data.items[i].store_items[j].product_price = (data.cart_data.items[i].store_items[j].payed_price / data.cart_data.items[i].store_items[j].quantity_ordered) 
	            data.subtotal = data.subtotal + (data.cart_data.items[i].store_items[j].product_price * data.cart_data.items[i].store_items[j].quantity_ordered);
          	}
          	if(data.cart_data.items[i].coupon_discount) data.total_coupon_discount = data.total_coupon_discount + data.cart_data.items[i].coupon_discount;
        }
        data.total = (data.cart_data.subtotal + data.sales_tax + data.vat + data.total_coupon_discount - data.shippingCost).toFixed(2);
    }
    return data;
} 