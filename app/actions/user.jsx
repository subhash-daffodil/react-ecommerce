import Redux from 'redux';

export const SIGNUP = 'SIGNUP'
export const LOGIN = 'LOGIN'
export const SET_USER = 'SET_USER'
export const DELETE_USER = 'DELETE_USER'
export const IS_LOGGEDIN = 'IS_LOGGEDIN'
export const ME = 'ME'

export function signUp(text) {
	return {type: SIGNUP, text}
}

export function login(filter) {
	return {type: LOGIN, filter}
}

export function setUser(text, cb) {
	return {type: SET_USER, text, cb}
}

export function deleteUser(index) {
	return {type: DELETE_USER, index}
}

export function isLoggedIn(index) {
	return {type: IS_LOGGEDIN, index}
}

export function me(index) {
	return {type: ME, index}
}