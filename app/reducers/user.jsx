import API from "../common/api.jsx";
import cookie from 'react-cookie';
import conifg from "../common/config.jsx";
import { signUp, login, setUser, deleteUser, isLoggedIn, me } from '../actions/user.jsx'
import objectAssign from 'object-assign'
export default function user(state = {}, action) {
  let auth_user = null
  switch(action.type) {
    case 'SIGNUP':
      return API.post(baseAPIUrl + 'signup', item, cb);
    case 'LOGIN': 
      return API.post(baseAPIUrl + 'login', item, cb);
    case 'SET_USER':
      if(action.text) {
        auth_user = action.text
        cookie.save('user', action.text);
      } else {
        auth_user = cookie.load('user') ? cookie.load('user') : null;
      }
      return objectAssign({}, state, {
        authUser: auth_user
      });
    case 'DELETE_USER':
      reactCookie.remove('user');
      return objectAssign({}, state, {
        authUser: false
      });
    case 'IS_LOGGEDIN':
      if(state.authUser) {
        if(typeof(state.authUser) === "string") {
          return JSON.parse(state.authUser);
        } else {
          return state.authUser;
        }
      } else {
        return false;
      }
    case 'ME':
      return API.get(baseAPIUrl + "users/me", null, cb);
    default: 
      return state
  }
}