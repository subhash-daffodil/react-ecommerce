import { combineReducers } from 'redux'

import cartReducer from './cart.jsx'
import categoryReducer from './category.jsx'
import homeReducer from './home.jsx'
import userReducer from './user.jsx'

const ECommerce = combineReducers({
  	cartReducer,
  	categoryReducer,
  	homeReducer,
  	userReducer
})

export default ECommerce