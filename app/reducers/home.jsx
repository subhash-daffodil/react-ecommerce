import productActions from '../actions/home.jsx'

import API from "../common/api.jsx";
import config from "../common/config.jsx";
import objectAssign from 'object-assign'

let baseAPIUrl = config.baseApiUrl;


export default function home(state = {}, action) {
    
  switch(action.type) {
    case 'SAVE_STORE': 
      return objectAssign({}, state, {store: action.store})
    case 'SAVE_CATEGORIES': 
      return objectAssign({}, state, {categories: action.categories})
    /*to save the values of trending products to state */
    case 'SAVE_TRENDINGPRODUCTS':
      return objectAssign({},state, {trendingProducts: action.trendingProducts});
    /*to save the values of newly arrived products to state*/
    case 'SAVE_ARRIVALPRODUCTS':
      return objectAssign({},state,{arrivalProducts: action.arrivalProducts});
    /*to save the values of products to state while scrolling for more*/  
    case 'SAVE_SCROLLPRODUCTS' :
      return objectAssign({},state,{scrollProducts: action.scrollProducts});  
    default: 
     return state;
  }
}