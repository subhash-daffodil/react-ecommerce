import { addCart, getCart, evaluateCart, removeItem, reviewCart, updateCart } from '../actions/cart.jsx'
import objectAssign from 'object-assign'
export default function cart(state = {}, action) {
  switch(action.type) {
    case 'SAVE_CART': 
      return objectAssign({}, state, {cart: action.cart})
    default: 
      return state
  }
}