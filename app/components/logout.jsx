import React, { Component } from 'react';
import store from '../store.jsx';
import { deleteUser } from '../actions/user.jsx';

export default class Logout extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showUser: false,
			userName: null
		}
	}

	componentWillMount() {
		this.setState({"userName": this.props.userName});
	}

	componentWillReceiveProps(newProps) {
    var user = store.getState().userReducer.authUser;
    if(user && typeof(user) === "string") {
      user = JSON.parse(user);
    }
    if(user && Object.keys(user).length) {
      var userName;
      if(user.username) {
        userName =  user.username;
        userName = userName.indexOf("@") == -1 ? userName : userName.substring(0, userName.indexOf("@"));
      } else {
        userName =  user.first_name + " " + user.last_name;
      }
      this.setState({
        userName : userName
      });
    }
	}

	showlogout_popup () {
   	this.setState({
      showUser : true
   	});
  }

  hidelogout_popup () {
   	this.setState({
      showUser : false
   	});
  }

  showDialogBox() {
    this.props.onOpenCloseDialog(true);
  }

  logout() {
    var that = this;
    store.dispatch(deleteUser());
    this.setState({
      userName : ''
    });
    that.hidelogout_popup();
    that.props.onSwitchUser(null);
  }

	render() {
		let logout_popup;
    let userData;
    if(this.state.showUser) {
      logout_popup = (
        <div className="popup_1 col-xs-12 pull-right" id="logout_popup" onMouseEnter={this.showlogout_popup.bind(this)} onMouseLeave={this.hidelogout_popup.bind(this)}>
        <div className="col-xs-12 none">  
            <div className="col-xs-12 padding_sm">{this.state.userName}</div>
            <div className="col-xs-12 padding_sm"><button className="btn_3 ng-binding">My Account</button></div>
            <div className="col-xs-12 padding_sm"><button className="btn_3 ng-binding">Change Password</button></div>
            <div className="col-xs-12 padding_sm"><button className="btn_3 ng-binding">My Orders</button></div>
            <div className="col-xs-12 padding_sm"><button className="btn_3 ng-binding">My Wishlists</button></div>
            <div className="col-xs-12 padding_sm" onClick={this.logout.bind(this)}><button className="btn_3 ng-binding">logout</button></div>
        </div>
      </div>
      )
    }

    if(this.state.userName) {
      userData = ( <a className="login_btn col-xs-12 limit text_3" onClick={this.showlogout_popup.bind(this)} onMouseLeave={this.hidelogout_popup.bind(this)} >{this.state.userName}</a> )
    } else {
      userData = ( <a className="login_btn col-xs-12 limit text_3" onClick={() => {this.showDialogBox()}}>Login</a> )
    }
		return (
			<div>
				<div className="col-xs-9 pull-right f8 text-right none margin-top-3">
		      {userData}
		    </div>
		    {logout_popup}
		  </div>
		)
	}
}