import React, { Component } from 'react';
import IconRating from '../lib/IconRating.js';
import config from "../common/config.jsx";
import API from "../common/api.jsx";

let baseAPIUrl = config.baseApiUrl;

export default class Product extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      productId: this.props.params.productId,
      isGetProduct : false,
      productInfo: {},
      selectedvariatent: {},
      quantity: 1,
      showFullLength: false,
      showReviewPopUp: false,
      showPostReviewComp: false
    };
  }

  componentWillReceiveProps() {
    // if(this.props.params.productId != this.state.productId) {
      this.setState({
        productId: this.props.params.productId
      }, function() {
        this.getProduct()
      }); 
    // }
  }
  
  componentWillMount() {
    this.getProduct();
  }

  getProduct() {
    var self = this;
    var productId = this.props.params.productId;
    API.get(baseAPIUrl + "product/" + productId, null, (err, result ,more) => {
      if(err || result === 'error' || more && more.responseText.indexOf('error') > -1) 
        return console.log("Error :", err);
      else {
        self.setState({
          isGetProduct: true,
          productInfo: result,
          selectedvariatent: result.variations[0],
          quantity: 1
        });
      }
    });
  }

  componentWillUnmount() {
    var self = this;
    self.setState({
      isGetProduct: false,
      productInfo: {}
    });
    $('.zoomContainer').remove();
  }

  zoomImage(){
    $("#selectedImg").elevateZoom({
      cursor: 'crosshair',
      responsive: true,
      scrollZoom: true,
      zoomWindowOffetx: 20,
      zoomWindowOffety: 0,
      zoomWindowWidth: 500,
      zoomWindowHeight: 500,
      lenszoom: true,
      tint: true,
      tintColour: '#F90',
      tintOpacity: 0.5
    });
  }
  
  changeSelectedVariatentByImgClick(event) {
    var target_id = event.target.id;
    var productInfo = this.state.productInfo;
    var pos = target_id.slice(target_id.indexOf("_") + 1);
    var selectedvariatent = productInfo.variations[pos];

    if(selectedvariatent && Object.keys(selectedvariatent).length   ) {
      selectedvariatent.variantValue = {};
      var temp = selectedvariatent.name.split('-');
      for (var i = 0; i < productInfo.variant.length; i++) {
        for (var j = 0; j < temp.length; j++) {
          if(productInfo.variant[i].accepted_value.indexOf(temp[j]) != -1) {
            var key = productInfo.variant[i].modifier
            selectedvariatent.variantValue[key] = temp[j]
            break;
          }
        };
      }
      this.setState({
        selectedvariatent : selectedvariatent
      })
    }
  }

  changeSelectedVariatentBySelectOption(event, key) {
    // for(var i = 0; i< $scope.product.variations.length; i++) {
    //   var flag=0;
    //   for(var j = 0; j < $scope.product.variant.length; j++)
    //     if($scope.product.variant[j].modifier == $scope.product.variations[i].attributes[j].value) flag++;
    //     if(flag == $scope.product.variant.length) {
    //       $scope.selectedItem = $scope.product.variations[i];
    //       $scope.selectChanged($scope.selectedItem,0);
    //     }
    // }
  }

  changeItemQuantity(event) {
    var value = event.target.value;
    this.setState({quantity: value});
  }

  toggleDescriptionLength() {
    var showFullLength = this.state.showFullLength;
    this.setState({
      showFullLength : !showFullLength
    });
  }

  showReviewPopUp() {
    this.setState({
      showReviewPopUp : true
    });
  }

  hideReviewPopUp() {
    this.setState({
      showReviewPopUp : false
    });
  }

  showPostReviewPopUp() {
    this.setState({
      showPostReviewComp : true
    });
    if(!this.props.loggedUser) {
      this.props.onOpenCloseDialog(true);
    }
  }

  hidePostReviewPopUp() {
    this.setState({
      showPostReviewComp : false
    });
  }

  addingToCart() {
    var product = this.state.productInfo;
    var currentItem = this.state.selectedvariatent;
    var quantity_ordered = parseInt(this.state.quantity ? (this.state.quantity < 1 ? 0 : this.state.quantity) : 1);
    var item = { items: [{
        name : product.name,
        product_code: product.product_code,
        description : product.description,
        store_code : config.store_code,
        category_code : product.category_code,
        item_code : product.product_code,
        item_image : currentItem.Assets.imagelinks[0].image,
        variation_description : currentItem.unitofmeasure,
        unit_of_measure : currentItem.unitofmeasure,
        variation_id: currentItem._id,
        payed_price : currentItem.Pricing.offerPrice,
        quantity_ordered : quantity_ordered,
        extended_price : currentItem.Pricing.regularPrice,
        applied_discount : currentItem.Pricing.discountAmount,
        variant_type : product.variant_type,
          // todo
            variation_name : currentItem.name,
            attributes : currentItem.attributes,
        VATrate : product.taxrates.VATrate,
        salestaxrate : product.taxrates.salestaxrate, 
        coupons : []
    }]};
    this.props.updateCart(item, function(data){
      if(data){
        console.log("cart updated!!");
      }
    });
  };

  render() {
    var self = this;
    var productInfo = this.state.productInfo;
    var selectedvariatent = this.state.selectedvariatent;
    var quantity = this.state.quantity;
    var showFullLength = this.state.showFullLength;
    var baseAPIUrl = config.baseApiUrl;
    var sliderButtons = null;
    var galleryArray = [];
    var imageArray = [];
    var priceComponent = [];
    var selectVariatentComp = null;
    var addToCartButton = null;
    var productDescription = null;
    var reviewComponent = [];
    var reviewPopUp = null;
    var postReviewComp = null;

    if(this.state.isGetProduct) {
      $('#loadingDiv').css({'display':'none'});
      var variationArray = productInfo.variations;
      var variationArrayLength = variationArray.length;
      var galleryLength = Math.floor(variationArrayLength / 4 + (((variationArrayLength % 4) > 0) ? 1 : 0));
      
      if(galleryLength > 1) {
        sliderButtons =(
          <span>
            <a className="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span className="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
              <span className="sr-only">Previous</span>
            </a>

            <a className="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span className="glyphicon glyphicon-chevron-right" aria-hidden="true" ></span>
              <span className="sr-only">Next</span>
            </a>
          </span>
        )
      }

      for(var i = 0; i < galleryLength; i++) {
        for(var j = 0; j < 4; j++) {
          if(i*4+j > variationArrayLength - 1 || !variationArray[i*4+j].Assets.imagelinks[0]) {
            break;
          }
          imageArray.push ((
            <div key={"img_" + i + "_" + j} className="col-xs-3 padding_sm" onClick={(event) => {this.changeSelectedVariatentByImgClick(event)}}>
              <a><img className="img-responsive" id={"img_" + (i*4 + j)} alt={variationArray[i*4+j].Assets.imagelinks[0].alt} src={baseAPIUrl + variationArray[i*4+j].Assets.imagelinks[0].image} /></a>
            </div>
          ));
        }
        galleryArray.push ((
          <div key={"imgDiv_" + i} className={"item " + (i == 0 ? 'active' : '')}>
            {imageArray}
          </div> 
        ));
        imageArray = [];
      }
       
      var discountPrice = selectedvariatent.Pricing.discountAmount;
      var newPrice = selectedvariatent.Pricing.regularPrice - selectedvariatent.Pricing.discountAmount;
      var profitPercentage = ((discountPrice * 100) / selectedvariatent.Pricing.regularPrice).toFixed(2);
      priceComponent.push ((
        <span key={"newPrice"} className="price text_1 f4">{discountPrice > 0 ? "Sale: " : "Price: " }<b>${newPrice}</b></span>
      ));

      if(discountPrice > 0) {
        priceComponent.push ((
          <span key={"regularPrice"} className="f10 margin-left-15"><del>Price: ${selectedvariatent.Pricing.regularPrice}</del></span>
        ));
      }

      if(discountPrice > 0) {
        priceComponent.push ((
          <p key={"discount"} className="limit text_1 f10">You save: ${discountPrice} ({profitPercentage}% off)</p>
        ));
      }

      if(selectedvariatent.Pricing.offerPriceValidUpto) {
        var dateLimit = new Date(selectedvariatent.Pricing.offerPriceValidUpto);
        dateLimit = dateLimit.toDateString();
        priceComponent.push ((
          <p key={"offerPriceValidUpto"} className="limit error f10">Offer valid until: {dateLimit}</p>
        ));
      }

      if(productInfo.variant.length) {
        var variatentDataTr = productInfo.variant.map(function(variantname, index) {
          var variatentDataOption = variantname.accepted_value.map(function(optionData, index) {
            var selectedValue = selectedvariatent.variantValue ? selectedvariatent.variantValue[variantname.modifier] : null;
            if(selectedValue && optionData === selectedValue){
              return (
                <option key={index} value={optionData} label={optionData} selected>{optionData}</option>
              );
            } else {
              return (
                <option key={index} value={optionData} label={optionData} >{optionData}</option>
              );
            }
          });
          return (
            <tr key={index}>
              <td>
                {variantname.modifier}
                <select onChange = {(event) => {self.changeSelectedVariatentBySelectOption(event, variantname.modifier)}} >
                  {variatentDataOption}
                </select>
              </td>
            </tr> 
          );
        });

        selectVariatentComp = (
          <div className="col-xs-12 margin_lg f8 padding_sm ">
            <table width="50%">
              <tbody>
                {variatentDataTr}
              </tbody>
            </table>
          </div>
        );

      }

      if(selectedvariatent.Inventory.quantityavailable > 0) {
        addToCartButton = (
          <div id="addToCart">
            <div className="col-md-6 col-sm-12 col-xs-6 phone padding_sm">
              <button className="btn_1 col-xs-12 f5 text_2 border" onClick={() => {this.addingToCart()}}>
                <i className="glyphicon glyphicon-shopping-cart f5"></i>
                  <b>Add to Cart</b>
                </button>
            </div>
          </div>
        );
      }

      if((productInfo.description).length < 525) {
        productDescription = (<p className="f11">{productInfo.description}</p>)
      } else {
        if(showFullLength) {
          productDescription = (
            <p className="f11">{(productInfo.description).trim()}
              <a className="clickAction" onClick={() => {this.toggleDescriptionLength()}}>
                <span className="pull-right">Less</span>
              </a>
            </p>
          )
        } else {
          productDescription = (
            <p className="f11">{(productInfo.description).substring(0, 525).trim()+"..."}
              <a className="clickAction" onClick={() => {this.toggleDescriptionLength()}}>
                <span className="pull-right">More</span>
              </a>
            </p>
          )
        }
      }

      if(productInfo.review_count == 0){
        reviewComponent = (
          <div className="col-xs-12 none">
            <button className="btn_3 text_4 text-left" onClick={() => {this.showPostReviewPopUp()}}>Be the first to write a Review</button>
          </div>
        );
      } else {
        reviewComponent.push((
          <div key={"viewReview"} className="col-xs-12 none">
            <button className="btn_3 text_4" onClick={() => {this.showReviewPopUp()}}>
              {"From " + productInfo.review_count + " " + (productInfo.review_count == 1 ? "Review" : "Reviews")}
            </button>
          </div>
        ));
        reviewComponent.push((
          <div key={"giveReview"} className="col-xs-12 none">
            <button className="btn_3 text_4 text-left" onClick={() => {this.showPostReviewPopUp()}}>Click to Write a Review</button>
          </div>
        ));
      }

      if(this.state.showReviewPopUp) {
        reviewPopUp = ( <ReviewPopUp onHideReviewPopUp={() => {this.hideReviewPopUp()}} productInfo={productInfo}/>);
      }

      if(this.state.showPostReviewComp && this.props.loggedUser) {
        postReviewComp = ( <PostReviewComp refreshProduct={() => {this.getProduct()}} onHidePostReviewPopUp={() => {this.hidePostReviewPopUp()}} loggedUser={this.props.loggedUser} productInfo={productInfo}/>);
      }

      return(
        <div className="container">
        <div className="col-xs-12 padding_lg" id="product">
          <div className="col-xs-12 none">

            <div className="col-md-4 col-sm-5 col-xs-12 none">
              <div className="myId col-xs-12 padding_sm" onMouseEnter={() => {this.zoomImage()}}>
                <div>
                  <span id="ex1" style={{"position":"relative", "overflow":"hidden"}}>
                    <img id="selectedImg" alt="" src={baseAPIUrl + selectedvariatent.Assets.imagelinks[0].image}/>
                  </span>
                </div>
              </div>

              <div className="col-xs-12 none">
                <div id="myCarousel" className="carousel slide">
                  <div className="carousel-inner"> 
                    {galleryArray}
                  </div>
                  {sliderButtons}
                </div>
              </div>
            </div>

            <div className="col-md-8 col-sm-7 col-xs-12">
              <div className="col-xs-12 f1 padding_sm border-bottom-1">{productInfo.name}</div>
              
              <div className="col-xs-12 padding_sm">
                {priceComponent}
              </div>

              {selectVariatentComp}

              <div className="col-xs-12 padding_sm">
                  <div className="col-xs-12 f10 padding_lg">
                    <div className="pull-left none f9">
                      <span>Quantity : </span>
                      <input className="input_sm ng-pristine ng-untouched ng-valid ng-valid-min" type="number" name="quantity_ordered" min="1" onChange={(event) => {this.changeItemQuantity(event)}} placeholder="1"/>
                    </div>
                    <div className="none limit text_1 f8 padding_sm pull-left ">
                      <span className={selectedvariatent.Inventory.quantityavailable > 0 ? "In Stock" : "error"}>{selectedvariatent.Inventory.quantityavailable > 0 ? "In Stock" : "Out of Stock"}</span>
                    </div>
                  </div>
              </div> 

              <div className="col-xs-12 none cart_bttn">
                {addToCartButton}
                <div className="col-md-6 col-sm-12 col-xs-6 phone padding_sm ">
                  <button className="pull-right btn_2 col-xs-12 f5 text_1 border">
                    <i className="glyphicon glyphicon-heart f5"></i>
                    <b>Add to List</b>
                    <span className="glyphicon glyphicon-collapse-down margin_md f10 pull-right"></span>
                  </button>
                </div>
              </div>

              <div className="col-xs-12 limt text-justify margin_lg padding_sm">
                <p className="f8" style={{"marginBottom":"0px"}}><b>Product Description:</b></p>
                {productDescription}
              </div>

            </div>
          </div>



          <div className="col-sm-12 col-xs-12 padding_sm f8 greyBack">
            <div className="col-xs-12  none">

              <div className="col-xs-12 border_b padding_sm">
                <IconRating className="rating col-xs-4 none" toggledClassName="glyphicon glyphicon-star" untoggledClassName="glyphicon glyphicon-star-empty" currentRating={productInfo.overallrating} viewOnly="true" />  
                <div className="col-xs-8 ">
                  {reviewComponent}
                </div>
              </div>


              <div className="col-xs-12 border_b padding_sm ">
                <div className="col-xs-4 none"><b>Delivery By: </b></div>
                <div className="col-xs-8 ">{"Usually ships within " + productInfo.shipping.level1.daystodeliver[0] + " to " + productInfo.shipping.level2.daystodeliver[1] + " business days."}</div>
              </div>

              <div className="col-xs-12 border_b padding_sm ">
                <div className="col-xs-4 none"><b>Others: </b></div>
                <div className="col-xs-8 ng-binding">Others</div>
              </div>


              <div className="col-xs-12 border_b padding_sm">
                <div className="col-xs-4 none "><b>Share: </b></div>
                <div className="col-xs-8">
                  <div className="social_icon">
                    
                    <span className="st_facebook_large" data-displaytext="Facebook" data-st_title="Daffo:" data-st_url="http://epikko.daffodilapps.com:8000/product/FOUTDOOR-NATALIE-PRINT-PILLOW89" data-st_processed="yes">
                      <span style={{"textDecoration":"none", "color":"#000000","display":"inline-block","cursor":"pointer"}} className="stButton">
                      </span>
                    </span>

                    <span className="st_twitter_large" data-st_url="http://epikko.daffodilapps.com:8000/Fproduct/FOUTDOOR-NATALIE-PRINT-PILLOW89" data-st_title="Daffo:" data-st_processed="yes">
                      <span style= {{"textDecoration":"none", "color":"#000000","display":"inline-block","cursor":"pointer"}} className="stButton">
                      </span>
                    </span>

                    <span className="st_pinterest_large" data-displaytext="Pinterest" data-st_title="Daffo:" data-st_url="http://epikko.daffodilapps.com:8000/product/FOUTDOOR-NATALIE-PRINT-PILLOW89" data-st_processed="yes">
                      <span style={{"textDecoration":"none", "color":"#000000","display":"inline-block","cursor":"pointer"}} className="stButton">
                      </span>
                    </span>
                  </div>
                </div>
              </div>                
            </div>
          </div>
          {reviewPopUp}
          {postReviewComp}
        </div>
       </div> 
      )
    } else {
      $('#loadingDiv').css({'display':'block'});
      return null;
    }
  }
}

class ReviewPopUp extends Component {
  
  closeReviewPopUp() {
    this.props.onHideReviewPopUp();
  }

  render() {  
    var baseAPIUrl = config.baseApiUrl;
    var productInfo = this.props.productInfo;
    var reviewScoreComp = null;

    var recent_reviews = productInfo.recent_reviews;
    reviewScoreComp = recent_reviews.map(function(review, index) {
      return(
        <div key={index} className="col-xs-12 padding_md">
          <div className="col-xs-12 bg_1 padding_sm">
            <div className="pull-left z_black f9">{review.review_title}</div>
            <div className="col-xs-6 none pull-right text-right z_black f9">
              <span className="col-xs-7 none limit">BY {review.creator_nickname} </span>
              <IconRating className="rating col-xs-5 none" toggledClassName="glyphicon glyphicon-star" untoggledClassName="glyphicon glyphicon-star-empty" currentRating={review.score} viewOnly="true"/>
            </div>
          </div>
          <div className="col-xs-12">{review.review_text}</div>
        </div>
      );
    });

    return (
      <div id="ngdialog8" className="ngdialog ngdialog-theme-plain ">
        <div className="ngdialog-overlay"></div>
        <div className="ngdialog-content">
          <div className="ratingPopUp" id="rating">
            <div className="bg_2 col-xs-12 none border">
              <div className="col-xs-12">
                <div className="text-center z_black f7 padding_md">
                  <b>Product Rating and Reviews</b>
                </div>
                <div className="col-xs-3 buy_img padding_sm">
                  <img alt={productInfo.variations[0].Assets.imagelinks[0].alt} src={baseAPIUrl + productInfo.variations[0].Assets.imagelinks[0].image} />
                </div>
                <div className="col-xs-9 padding_lg">
                  <div className="z_black limit f8">{productInfo.name}</div>
                </div>
                <div className="pull-right">
                    <div className="limt f9 z_black">
                        <span>Overall Rating</span>
                        <IconRating className='rating' toggledClassName="glyphicon glyphicon-star" untoggledClassName="glyphicon glyphicon-star-empty" currentRating={productInfo.overallrating} viewOnly="true"/>
                    </div>
                </div>
              </div>
              <div className="col-xs-12">
                  <div className="pull-left z_black f8 padding_md">Recent Reviews</div>
                  <div className="pull-right padding_md">
                    <a>
                      Click to see full Reviews page
                    </a>
                  </div>
              </div>

              <div className="col-xs-12 none">
                {reviewScoreComp}
              </div>
            </div>
            <div className="ngdialog-close" onClick={() => {this.closeReviewPopUp()}}></div>
          </div>
        </div>
      </div>
    );
  }
}

class PostReviewComp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      creator_nickname : null,
      recommend : "true",
      review_title : null,
      review_text : null,
      score : 1
    };
  }
  
  closeReviewPopUp() {
    this.props.onHidePostReviewPopUp();
  }

  limitText(e, countSpanName, limitNum) {
    var limitFieldValue = $(e.target).val();
    var limitCount = $('span#' + countSpanName);
    if(limitFieldValue.length > limitNum) {
      var limitValue = limitFieldValue.substring(0, limitNum);
      $(e.target).val(limitValue);
    } else {
      this.setState({
        review_text : limitFieldValue
      });
      var charLeft = limitNum - limitFieldValue.length;
      limitCount.html((charLeft < 1 ? "Reached max character limit" : charLeft + " " + (charLeft > 1 ? "Characters Left" : "Character Left" )));
    }
  }

  setStateValue(event, key) {
    var value = event.target.value;
    var changeObj = {};
    changeObj[key] = value;
    this.setState(changeObj);
  }

  setScore(score) {
    this.setState({
      score : score
    });
  }

  postReview() {
    var self = this;
    var product_code = this.props.productInfo.product_code;
    var creator_nickname = (this.state.creator_nickname ? this.state.creator_nickname : this.props.loggedUser.username);
    var recommend = this.state.recommend;
    var review_title = this.state.review_title;
    var review_text = this.state.review_text;
    var score = this.state.score;
    var check = true;

    if(!(review_title && review_title.length)) {
      check = false;
      $("input[name=Title]").parent().addClass('has-error');    
    }

    if(!(review_text && review_text.length)) {
      check = false;
      $("textarea[name=reviewText]").parent().addClass('has-error');
    }

    if(check) {
      $('.has-error').removeClass('has-error');
      var item = {
        review_text : review_text,
        creator_nickname: creator_nickname,
        review_title : review_title,
        recommend : recommend,
        score : score,
        product_code : product_code
      };
      API.post(baseAPIUrl + "product/" + item.product_code + "/review/", item, (err, result, more) => {
        if(err || result === 'error' || more && more.responseText.indexOf('error') > -1) {
          console.log("Error :", err);
        } else {
          self.props.refreshProduct();
          self.closeReviewPopUp();
        }
      });
    }
  }

  render() {  
    var self = this;
    var baseAPIUrl = config.baseApiUrl;
    var productInfo = this.props.productInfo;
    var loggedUser = this.props.loggedUser;
    var reviewScoreComp = null;

    return (
      <div id="ngdialog8" className="ngdialog ngdialog-theme-plain ">
        <div className="ngdialog-overlay"></div>
        <div className="ngdialog-content">
          <div className="ratingPopUp" id="rating">
            <div className="bg_2 col-xs-12 none border">
              <div className="col-xs-12">
                <div className="col-xs-3 buy_img padding_sm">
                  <img alt={productInfo.variations[0].Assets.imagelinks[0].alt} src={baseAPIUrl + productInfo.variations[0].Assets.imagelinks[0].image} />
                </div>
                <div className="col-xs-9 padding_lg">
                  <div className="z_black limit f7">{productInfo.name}</div>
                </div>
              </div>
              <div className="col-xs-12">
                <div className="col-xs-12 padding_sm z_black f10">
                  Your first name will appear on your review or you can select a nickname instead.
                </div>
              </div>
              <div className="col-xs-12 border_b">
                <form>
                  <div className="col-xs-12 padding_sm">
                    <i className="glyphicon glyphicon-user form-i"></i>
                    <input name="Nickname" placeholder="nickname" onChange={(event) => {this.setStateValue(event, 'creator_nickname')}} />
                  </div>

                  <div className="col-xs-12 padding_sm z_black f10">
                    <b>Rate this product - </b> Click in the desired star rating
                  </div>

                  <IconRating className="col-xs-12 none rating" toggledClassName="glyphicon glyphicon-star" untoggledClassName="glyphicon glyphicon-star-empty" onChange={(number) => {this.setScore(number)}} currentRating="1" />

                  <div className="col-xs-12 padding_sm z_black f10">
                    <span>Would you recommend this product?</span>
                    <span> <input type="radio" name="1" value="true" onChange={(event) => {this.setStateValue(event, 'recommend')}} checked={"true" === this.state.recommend} />Yes</span>
                    <span> <input type="radio" name="1" value="false" onChange={(event) => {this.setStateValue(event, 'recommend')}} checked={"false" === this.state.recommend} />No</span>
                  </div>

                  <div className="col-xs-12 padding_sm form-group">
                    <i className="glyphicon glyphicon-text-width form-i"></i>
                    <input className="form-control" name="Title" placeholder="Title" type="Title" onChange={(event) => {this.setStateValue(event, 'review_title')}} required />
                  </div>

                  <div className="col-xs-12 padding_sm form-group">
                    <span><b>Review</b> (Max 2000 Characters)</span>
                    <textarea name="reviewText" className="col-xs-12 form-control" onChange={(event) => {this.limitText(event, 'countdown', 2000)}}></textarea>
                    <span className="pull-right" id="countdown"></span>
                  </div>

                  <div className="col-xs-12 padding_sm text-right">
                    <button className="btn_4 text_2 border" type='button' onClick={() => {this.postReview()}}>Submit</button>
                    <span> Or </span>
                    <button className="btn_3 text_4 ngdialog-close close_hide" type='button' onClick={() => {this.closeReviewPopUp()}}>Cancel</button>
                  </div>
                </form>
              </div>
            </div>
            <div className="ngdialog-close" onClick={() => {this.closeReviewPopUp()}}></div>
          </div>
        </div>
      </div>
    );
  }
}