import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import  DebounceInput from 'react-debounce-input'

import config from "../common/config.jsx";
import * as cartActions from '../actions/cart.jsx';
import userActions from '../actions/user.jsx';
import IfRender from './ifRender.jsx';

class ViewCart extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      cart: null,
      user: null
    };
  }
  
  componentWillMount() {
    this.updateState(null);
  }
  componentWillReceiveProps(props) {
    this.updateState(props);
  }
  updateState(props) {
    let { cartReducer, user } = (props && Object.keys(props).length) ? props : this.props;
    this.setState({
      cart: cartReducer.cart
    });
    if(user && typeof(user) === "string") {
      user = JSON.parse(user);
    }
    if(user && Object.keys(user).length) {
      this.setState({user: user});
    }
  }

  removeItem(item,store) {
    var itemData = {id: item._id, item: item, storeitem: store};
    const { dispatch } = this.props;
    dispatch(cartActions.removeItemAPI(itemData));
    this.props.fetchCart();
  }
  qtyChanged(item,quantity_ordered,store_code) {
    $('#loadingDiv').css({'display':'block'});
    item.store_code = store_code;
    item.quantity_ordered = quantity_ordered;
    const { dispatch } = this.props;
    dispatch(cartActions.qtyChangedAPI({item: item}));
    $('#loadingDiv').css({'display':'none'});
    this.props.fetchCart();
  }

  userStatus() {  
    if(this.state.user && Object.keys(this.state.user).length) {
      this.context.router.push("/checkout");
    } else {
      this.props.onOpenCloseDialog(true);
    }
  }

  render() {
    var self = this;
    var cart_data = this.state.cart ? this.state.cart.cart_data : null;
    var host = config.baseApiUrl;
    
    
    if(cart_data && cart_data.items && cart_data.items[0]) {
      var items  =  cart_data.items[0].store_items.map(function (item,index) {
        return (
          <div key={index} id={"viewcart" + index}  className="col-xs-12 border_b padding_md">
            <div className="col-md-2 col-sm-2 col-xs-4 none text-center buy_img phone">
              <Link to={"product/"+item.item_code}>
                <img src={host + item.item_image}/>
              </Link>
            </div>
            <div className="col-md-5 col-sm-4 col-xs-5 none phone">
              <div className="col-xs-12 none">
                <div className="col-xs-12 none z_black limit f10">
                  <Link className="z_black" to={"product/"+item.item_code}>
                    {item.name}
                  </Link>
                </div>
              </div>
              {item.variant_type=='P' ? (
                <div className="col-xs-12 none limit" >
                  <b>{"package"}</b> : {item.variation_name}
                </div>) : ''}
                  
              { item.attributes.length > 0 ? (<Attributes data={item.attributes} />) : null}
              <div className="col-xs-12 none">
                <div className="col-xs-12 visible-xs none limit text_1 f11">{config.currency + config.toFixed(item.product_price)}</div>
                <div className="col-xs-12 visible-xs none f11">Qty <DebounceInput className="input_sm padding_xs" value={item.quantity_ordered} debounceTimeout={100} type="number" min="1" onChange={(event)=> self.qtyChanged(item,event.target.value,cart_data.items[0].store_code)} /></div>
                { self.state.user ?  (<div className="col-xs-12 none f11 visible-xs" style={{marginTop: "8px"}} ><span onClick={() => self.removeItem(item, cart_data.items[0])}><span className="glyphicon glyphicon-trash removeAction"></span>REMOVE</span></div>) : null } 
              </div>
            </div>
            <div className="col-md-5 col-sm-6 col-xs-3 none phone">
              <div className="col-xs-12 visible-xs none text-right limit f9">{config.currency + config.toFixed(item.payed_price)}</div>
              <div className="col-xs-12 none hidden-xs">
                <div className="col-xs-3 none limit text_1 f8">{config.currency + config.toFixed(item.product_price)}</div>
                  <div className="col-xs-3 none">Qty <DebounceInput className="input_sm " value={item.quantity_ordered} debounceTimeout={2000} type="number" min="1" onChange={(event)=> self.qtyChanged(item,event.target.value,cart_data.items[0].store_code)} /></div> 
                  <div className="col-xs-5 none">
                    <div className="col-xs-9 none f8 text-right" >
                      {config.currency + config.toFixed(item.payed_price)}
                    </div>
                    { self.state.user ?  (<div className="col-xs-3 none f8 text-right" ><span onClick={() => self.removeItem(item, cart_data.items[0])} className="glyphicon glyphicon-trash removeAction"> </span></div>) : null } 
                 </div>
                </div>
              </div>
            </div>
        )   
      }) 
    }
    return (
      <form name="viewCartForm" noValidate> 
        <div className="col-xs-12 none" id="cart">
          <div className="col-xs-12 padding_lg border_b f7" ><b>Product Cart</b></div>
          <div className="col-xs-12 padding_lg border_b" >
            <div className="col-md-12 col-sm-12 col-xs-12 none pull-right b_none">
              {items}
            </div>
          </div>
          {cart_data && Object.keys(cart_data).length && !cart_data.items.length ? ( 
            <div>
              <IfRender showMe={!cart_data.items.length} data={<div className="col-xs-12 text-center f10" ><b>There are no items in this cart</b></div>} />
              <IfRender showMe={!cart_data.items.length} data={<div className="col-xs-12 padding_lg border_b text-center"><a className="btn_1 text_2 f9" href="" >continue_shopping</a></div>} />
            </div> 
          ) : null} 
          {cart_data && cart_data.items.length ? ( 
             <IfRender showMe={cart_data.items.length} data={<div className="col-md-6 col-sm-6 col-xs-12 padding_md pull-right f8">
               <div className="col-md-8 col-sm-6 col-xs-8 text-right none">
                 <div>Subtotal:  </div>
                 <div>Shipping: </div>
                 <div>Sales_tax: </div>
                 <div>Vat: </div>
                 <div>Discount: </div>
                 <div className="z_black"><b>Total:</b></div>
               </div>
               <div className="col-md-4 col-sm-6 col-xs-4 text-right none">
                 <div><b>{config.currency + config.toFixed(this.state.cart.cart_data.subtotal)}</b></div>
                 <div><b>{config.currency + config.toFixed(this.state.cart.shippingCost)}</b></div>
                 <div><b>{config.currency + config.toFixed(this.state.cart.sales_tax)}</b></div>
                 <div><b>{config.currency + config.toFixed(this.state.cart.vat)}</b></div>
                 <div><b>{config.currency + config.toFixed(this.state.cart.total_coupon_discount)}</b></div>
                 <div  className="z_black"><b>{config.currency + config.toFixed(this.state.cart.total)}</b></div>
               </div>

               <div className="col-md-8 col-sm-6 col-xs-12 pull-right text-right padding_md" ><button className="btn_1 text_2 border" type="button" onClick={() => this.userStatus()}>Checkout</button></div>

             </div>} />
          ): null}
        </div>
      </form>
    )
  }
}

const mapStateToProps = (state) => {
  let user = (state.userReducer && Object.keys(state.userReducer).length) ? state.userReducer.authUser : {};
  return {cartReducer: state.cartReducer, user: user}
}

ViewCart.contextTypes = {
  router: React.PropTypes.object.isRequired
};
ViewCart = connect(mapStateToProps)(ViewCart)
export default ViewCart;


class Attributes extends Component { 

  render() {
    var attributes = this.props.data.map(function(attribute){      
        return (
              <div key={attribute._id} className="col-xs-12 none f11" >
                <b>{attribute.name}</b> : {attribute.value}
              </div>   
        )        
    })
    return (
      <div> 
        {attributes} 
      </div>
      )
  }
}