import React, { Component } from 'react';
import config from "../common/config.jsx";

export default class Contact extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      store : this.props.storeData
    };
  }

  componentWillMount() {
    var address = this.state.store.storeaddress;
    var searchAddress = address.addressLine1 + "+" + address.city + "+" + address.stateorprovince + "+" + address.country;
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + searchAddress + "&key=" + config.googleApiKey;
    $.ajax({
      type : 'GET',
      dataType: 'json',
      url: url.trim(),
      success: (result) => {
        var location = result.results[0].geometry.location
        var geocoder = new google.maps.Geocoder();
        var myLatlng = new google.maps.LatLng(location.lat, location.lng);
        var mapOptions = {
          zoom: 13,
          center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: 'location'
        });
      },
      error: (xhr, status, error) => {
        console.log("error: ", error); 
      },
      timeout: 10000
    });  
  }

  render() {
    var store = this.state.store;
    if(!store) {
      return null;
    } else {
      return (
        <div>         
          <section>
            <div className="col-xs-12 none margin_lg">
              <div className="col-xs-12 text-left f2 padding-bottom-10"><b>Contact Us</b></div>
              
              <div className="col-xs-12 f8 padding-bottom-10">
                  {store.storeaddress.addressLine1 + ", " + (store.storeaddress.city ? store.storeaddress.city : store.storeaddress.stateorprovince) + ", " +  (store.storeaddress.country)}
                  <a className="pull-right" href="">{store.contact.websitelink}</a>
              </div>
              <div className="col-xs-12">
                <div className="googleMap" id="map"></div>
              </div>
            </div> 
          </section>
        </div>
      );
    }
  }
}
