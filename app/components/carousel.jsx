import React, { Component } from 'react';
import config from "../common/config.jsx";

export default class Carousel extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let imageArray = this.props.imageArray;
    var host = config.baseApiUrl;
    var productsList = [];
    var products = null;
    var sliderButton = null;
    var showCarousel = null;
    
    if(imageArray && imageArray.length) {

      products = imageArray.map(function(slide, index) {
        productsList.push((
          <li key={index} data-target="#myCarousel" data-slide-to={index} className={index == 0 ? 'active' : ''}></li>
        ));
        
        return (
          <div  key={slide._id} className={"item carousel-div-height " + (index == 0 ? 'active' : '')}>
            <img className="carousel-img" alt={slide.alt} src={host + slide.image} style={{"margin":"auto"}} />
          </div>
        );
      });
    }

    if(imageArray && imageArray.length > 1) {
      sliderButton = (
        <span>
          <a className="left carousel-control custom-carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span className="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="right carousel-control custom-carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span className="glyphicon glyphicon-chevron-right" aria-hidden="true" ></span>
            <span className="sr-only">Next</span>
          </a>
        </span>
      );
    }

    showCarousel = (
      <div id="myCarousel" className="carousel slide" data-ride="carousel" data-interval="2500">
        <ol className="carousel-indicators customeCarousel-indicators">
          {productsList}
        </ol>

        <div className="carousel-inner" role="listbox">
          {products}
        </div>
        {sliderButton}
      </div>
    );

    return (
      <div className="col-xs-12 none carousel customCarousel">
        {showCarousel}
      </div>
    );
  }
}