import React from 'react';

import ProductList from "../components/productList.jsx";
import conifg from "../common/config.jsx";

export default class Store extends React.Component {
  
  render() {
    
    var store = this.props.data;
    var host = conifg.baseApiUrl;

    var products = store.Assets.images.map(function(slide, index) {
      return (
        <div key={slide._id}>
          <img  alt={slide.alt} src={host+slide.image} style={{"margin":"auto"}} />
        </div>
      );
    });
    return (
      <div className = "col-xs-12 none" id="store">
        <form name="storeForm">
          <div className="col-xs-12 none carousel">
             <carousel interval="myInterval">
                 <slide>
                     {products}
                 </slide>
            </carousel>
      
          </div>
          <section>
            <div  className="col-xs-12 padding_lg bg_1">
              <div className="col-xs-12 text-center none f7"><b>{store.name}</b></div>
              <div className="col-xs-12 text-center none"><b>{store.storeaddress.addressLine1}, {store.storeaddress.city ? store.storeaddress.city : store.storeaddress.stateorprovince}, {store.storeaddress.country} </b></div>
              <div className="col-xs-12 about text-center phone text-justify"><p>{store.about}</p></div>
              </div>
          </section>
          <ProductList data={store} />
          <section>
            <div className="col-xs-12 none margin_lg bg_1">
              <div className="col-xs-12 text-center f2"><b>About</b></div>
                
              <div className="col-xs-12 padding_lg">
              <div className="col-md-4 col-sm-4 col-xs-12 text-center f5"><b>{store.name}</b></div>
              <div className="col-md-8 col-sm-8 col-xs-12 f5">{store.storeaddress.addressLine1 + (store.storeaddress.city ? store.storeaddress.city : store.storeaddress.stateorprovince) +  (store.storeaddress.country)} 
              <a className="pull-right" href="">{store.contact.websitelink}</a></div>
              </div>
            </div>
              
          </section>
        </form>  
      </div>  
    );
  }
}