import React from 'react';
import { connect } from 'react-redux'
import * as homeActions from "../actions/home.jsx";
import Products from "./products.jsx";
import _ from "underscore"

let scroll_in_process = false;
let lastScrollTop = 0;
let skip = 0;
let limit = 6;
let listener;
class Home extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      productArray: [],
      trendingProducts: [],
      arrivalProducts: [],
      scrollProducts:[]
    };
  }
  componentWillMount() {
    /*to call the corresponding actions in the ../actions/home.jsx*/
    const { dispatch } = this.props;
    dispatch(homeActions.getTrendingProductsAPI());
    dispatch(homeActions.getArrivalProductsAPI());
    dispatch(homeActions.getScrollProductsAPI(skip,limit)); 
    listener = this.handleScroll.bind(this);
    window.addEventListener('scroll', listener);
  }
  componentWillReceiveProps(props) {
     /*to set the state of the trending products and newly arrived products in the corresponding components*/
    const {state} = props;
    let trendingProducts = state.trendingProducts;
    let arrivalProducts = state.arrivalProducts;
    this.setState({
      trendingProducts: trendingProducts,
      arrivalProducts: arrivalProducts
    });
  }
  componentWillUnmount () {
    scroll_in_process = false;
    lastScrollTop = 0;
    skip = 0;
    limit = 6;
    window.removeEventListener('scroll', listener);
  }
  handleScroll(event) {
    var self = this;
    var st = $(window).scrollTop();
    if (st > lastScrollTop) {
      if (!scroll_in_process && $("#scrollDiv").offset() && (st + $(window).height()) > $("#scrollDiv").offset().top) {
        scroll_in_process = true;
        /*to call the getScrollProductsAPI action to set the state of more products while scrolling, into the corresponding components*/
        const { dispatch } = self.props;
        skip = skip + limit;
        dispatch(homeActions.getScrollProductsAPI(skip,limit)); 
         let scrollProducts = self.props.state.scrollProducts;
          if(scrollProducts.length == 0) {
            console.log("Data End!!");
          } else {
            let productArray = this.state.productArray;
            for(let i = 0; i < scrollProducts.length; i++) {
              /*to avoid repeating values of products*/
              function containsObject(obj, list) {
                  let res = _.find(list, function(val){ return _.isEqual(obj, val)});
                  return (_.isObject(res))? true:false;
                  }
                  let isexits = containsObject(scrollProducts[i] , productArray);
                if(!isexits){
                      productArray.push(scrollProducts[i]);
                }
            }
            self.setState({
              productArray : productArray
            })
            scroll_in_process = false;
          }
      }
    }
    lastScrollTop = st;
  }
  render() {
    let isDataFetching = true;
    let newArrivalLargeProduct = null;
    if(this.state.trendingProducts && this.state.trendingProducts.length ) {
      isDataFetching = false;
    }
    if(isDataFetching) {
      $('#loadingDiv').css({'display':'block'});
      return null;
    } else {
      $('#loadingDiv').css({'display':'none'});
      return (
        <div>
          <Products products={this.state.trendingProducts} name="Trending Products" />
          <Products products={this.state.arrivalProducts} name="New Arrivals" />
          <Products products={this.state.productArray} name="More Popular Products" />
          <div id="scrollDiv">&#160;</div>
        </div>
      );
    }
  }
}
class ImageUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {file: '',imagePreviewUrl: ''};
  }
  _handleSubmit(e) {
    e.preventDefault();
    // TODO: do something with -> this.state.file
    console.log('handle uploading-', this.state.file);
  }
  _handleImageChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(file)
  }
  render() {
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    return (
      <div className="previewComponent">
        <form onSubmit={(e)=>this._handleSubmit(e)}>
          <input className="fileInput" type="file" onChange={(e)=>this._handleImageChange(e)} />
          <button className="submitButton" type="submit" onClick={(e)=>this._handleSubmit(e)}>Upload Image</button>
        </form>
        <div className="imgPreview">
          {$imagePreview}
        </div>
      </div>
    )
  }
}
/*to call the connect function to connect the Home component to store*/
const mapStateToProps = (state) => {
    return { state: state.homeReducer }
  }
Home = connect(mapStateToProps)(Home)
export default Home;
