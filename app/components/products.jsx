import React, { Component } from 'react';
import { Link } from 'react-router';
import config from "../common/config.jsx";
import IconRating from '../lib/IconRating.js';

export default class Products extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let host = config.baseApiUrl;
  	let products = this.props.products;
  	let sectionName = this.props.name;
  	return (
  		<section>
        {sectionName != "categoryProducts" ? (
        	<div className="col-xs-12 text-center none f5">
            <span>{sectionName}</span>
          </div>
        ) : null }
      	<div className="col-xs-12 padding_lg">
        {/*to prevent the product map undefined error*/}
        {products && products.length ? products.map((product, index) => {
          if((sectionName !== "More Popular Products") && (sectionName !== "categoryProducts")) {	
          	product = product.product_id;
          }
          if(product && Object.keys(product).length && product.variations[0].Assets.imagelinks[0]) {
            return (
            	<div key={index}>
      					{sectionName == "New Arrivals" && index==0 ? (
      						<div className="col-md-8 col-sm-8 col-xs-12 none category">
				            <Link className="text_3" to={'product/'+product.product_code} >
				              <div className="col-xs-12 border none text-center">
				                <div id="myCarousel" className="carousel slide" data-ride="carousel">
				                  <div className="carousel-inner" role="listbox">
				                    <div className="item active">
				                      <img className="first-slide" alt={product.variations[0].Assets.imagelinks[0].alt} src={host+product.variations[0].Assets.imagelinks[0].image} />
				                    </div>
				                  </div>
				                </div>
				              </div>
				              <div className="col-xs-8 none limit">{product.name.length > 22 ? product.name+'...':product.name}</div>
				              <div className="text-right col-xs-4 none limit text_1"><b>{"$"+product.variations[0].Pricing.regularPrice}</b></div>
				            </Link>
				          </div>
      					) : (
                  <div className="col-md-3 col-sm-4 col-xs-6 padding_sm category phone ng-scope">
                    <Link className="text_3" to={'product/'+product.product_code} >
                      <div className="col-xs-12 border none text-center">
                        <div id="myCarousel" className="carousel slide" data-ride="carousel">
                          <div className="carousel-inner" role="listbox">
                            <div className="item active">
                              <img className="first-slide" alt={product.variations[0].Assets.imagelinks[0].alt} src={host+product.variations[0].Assets.imagelinks[0].image} />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-xs-8 none limit">{product.name.length > 22 ? product.name+'...':product.name}</div>
                      <div className="text-right col-xs-4 none limit text_1"><b>{"$"+product.variations[0].Pricing.regularPrice}</b></div>
                    	{sectionName == "categoryProducts" ? (
                    		<IconRating className="col-xs-12 none rating" toggledClassName="glyphicon glyphicon-star" untoggledClassName="glyphicon glyphicon-star-empty" currentRating={product.overallrating} viewOnly="true" />
                    	) : ''}
                    </Link>
                  </div>
                )}
              </div>   
            );
          }
        }) : ''}
      	</div>
      </section>
  	)
  }
}