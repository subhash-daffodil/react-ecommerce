import React, { Component } from 'react';
import { connect } from 'react-redux'
import cookie from 'react-cookie';
import {Typeahead} from 'react-typeahead';
import _ from 'underscore';

import { qtyChangedAPI } from '../actions/cart.jsx';
import config from "../common/config.jsx";
import API from "../common/api.jsx";

let baseAPIUrl = config.baseApiUrl;
class Checkout extends Component {
  
  constructor(props) {
    super(props);
    this.getCity = _.debounce(this.getCity,"500");
    this.state = {
      shipping_address : {},
      name : "",
      last_name : "",        
      addressLine1 : "",
      addressLine2 : "",
      phone_number : "",
      shippingCityName : "",
      pin_code : "",
      newShippingAddress : false,
      use_address : "",
      saveShippingAddress : "",
      cities : [],
      firsttest: false,
      special_instruction_check : false,
      selection : "default",
      firstsection : true,
      coupon: {
          coupon_code:'',
          invalid_code:'',
          coupon_error:''
        },
      lastsection : false,
      card_selected : "",
      company_name: "",
      notification_email: "",
      same_billing_address: false,
      card_number: '',
      name_on_card: '',
      card_cvv: ''
    };
  }
 
  componentWillMount() {
    Stripe.setPublishableKey(config.PublishableStripeKey); 

    var self = this;
    var checkoutdata = cookie.load('checkoutdata');
    var checkout_billing_data = cookie.load('checkoutbillingdata');
    this.updateState(null);
    API.get(baseAPIUrl + "users/me", null, function(err,data,more) {

      if(data.username) {
        if (data.shippinglocations.length > 0) {
          var user_checkout = data;
          self.setState({
            user_checkout : user_checkout,
            multipleShippingAddress : true,
            useAddress :user_checkout.shippinglocations[0],
            selection :"saved_bill_address_and_cart_final",
            name :user_checkout.shippinglocations[0].first_name,
            last_name :user_checkout.shippinglocations[0].last_name,
            phone_number :user_checkout.shippinglocations[0].phoneno,
            notification_email :user_checkout.username,
            addressLine1 :user_checkout.shippinglocations[0].address_line1,
            addressLine2 :user_checkout.shippinglocations[0].address_line2,
            shippingCityName :user_checkout.shippinglocations[0].city,
            pin_code :user_checkout.shippinglocations[0].postalcode,
            firsttest : true,
            firstsection : true,
            middlesection : true,
            saveShippingAddress : true,
          })
          if(data.paymentmethods.length > 0) {
            var user_payment = data.paymentmethods;
            self.setState({
              user_payment : user_payment,
              change_card : user_payment[0],
              card_selected : true,
              already_cardnumber : true,
              saveCardDetails : false,
              card_number : "XXXX XXXX XXXX " + user_checkout.paymentmethods[0].masked_number,
              // $scope.expiry_month = 1;
              // $scope.expiry_year = 2017;
              middletest : true,  
              lastsection : true,
              same_billing_address : true,
            })
          }
        }
        else if(checkoutdata) {
          var user_checkout = (typeof checkoutdata == 'string') ?  JSON.parse(checkoutdata): checkoutdata;
          self.setState({
            user_checkout : user_checkout,
            name : user_checkout.name,
            last_name : user_checkout.last_name,
            addressLine1 : user_checkout.addressLine1,
            addressLine2 : user_checkout.addressLine2,
            phone_number : user_checkout.phone_number,
            shippingCityName : user_checkout.shippingCityName,
            pin_code : user_checkout.pin_code,
            notification_email : user_checkout.notification_email, 
          })
          if (checkout_billing_data) {
            var checkoutbillingdata = (typeof checkout_billing_data == 'string') ?  JSON.parse(checkout_billing_data): checkout_billing_data;
            self.setState({
              checkoutbillingdata : checkoutbillingdata,
              biller_name : checkoutbillingdata.biller_name,
              biller_last_name : checkoutbillingdata.biller_last_name,
              biller_addressLine1 : checkoutbillingdata.biller_addressLine1,
              biller_company_name : checkoutbillingdata.biller_company_name,
              biller_addressLine2 : checkoutbillingdata.biller_addressLine2,
              biller_phone_number : checkoutbillingdata.biller_phone_number,
              biller_city : checkoutbillingdata.biller_city,
              biller_pin_code : checkoutbillingdata.biller_pin_code,
              same_billing_address : checkoutbillingdata.same_billing_address,
            })
          }     
        } 
      } 
    });
  }
  
  componentWillReceiveProps(props) {
    this.updateState(props);
  }

  updateState(props) {
    let { cart } = (props && Object.keys(props).length) ? props : this.props;
    if(cart && Object.keys(cart).length) {
      this.setState({
        cart: cart,
        total: cart.total,
        cart_data : cart.cart_data
      });
    }
  }

  setStateValue(event, key) {
    var value;
    if(key == "special_instruction_check") {
      value = $('input:checkbox[name=special_instruction_check]:checked').val();
    } else if(key == "saveCardDetails") {
      value = $('input:checkbox[name=saveCardDetails]:checked').val();
    }
    else {
      value = event.target.value;
    }
    $('#'+key).remove()
    var changeObj = {};
    changeObj[key] = value;
    this.setState(changeObj);
  }

  setCityValue(val,key) {
    var changeObj = {};
    changeObj[key] = val;
    this.setState(changeObj);
    $("div[name="+key+"]").remove();
  }


  shipping_address (useAddress) {
    if(useAddress) {
        if((typeof useAddress)=="string") {
          this.setSate({
            use_address : JSON.parse(useAddress)
          })
        } else {
          this.setSate({
            use_address : useAddress
          })
        }
      if(this.state.newShippingAddress) {
        this.state.saveShippingAddress = true;
        this.state.name = this.state.use_address.first_name;
        this.state.last_name = this.state.use_address.last_name;
        this.state.addressLine1 = this.state.use_address.address_line1;
        this.state.addressLine2 = this.state.use_address.address_line2;
        this.state.phone_number = this.state.use_address.phoneno;
        this.state.shippingCityName = this.state.use_address.city;
        this.state.pin_code = this.state.use_address.postalcode;
      } else {
        this.state.saveShippingAddress = false;
        this.state.use_address.first_name = this.state.name   
        this.state.use_address.last_name = this.state.last_name ;
        this.state.use_address.address_line1 = this.state.addressLine1 ;
        this.state.use_address.address_line2 = this.state.addressLine2;
        this.state.use_address.phoneno = this.state.phone_number;
        this.state.use_address.city = this.state.shippingCityName;
        this.state.use_address.postalcode = this.state.pin_code;
      }
    } else {
      this.state.saveShippingAddress = false;
    } 
    var check = true; 
    isNotEmpty("name",this.state.name) ? null : (check = false) 
    isNotEmpty("last_name",this.state.last_name) ? null : (check = false)
    isNotEmpty("phone_number",this.state.phone_number) && isPhoneNumber("phone_number",this.state.phone_number) ? null : (check = false)
    isNotEmpty("addressLine1",this.state.addressLine1) ? null : (check = false)
    isNotEmpty("pin_code",this.state.pin_code) ? null : (check = false)
    isNotEmpty("notification_email",this.state.notification_email) && isEmail("notification_email",this.state.notification_email) ? null : (check = false)

    if (check && this.state.shippingCityName && (this.state.instructions || !this.state.special_instruction_check)) {        
        var data = {"name":this.state.name,"last_name":this.state.last_name,"addressLine1": this.state.addressLine1,
                    "addressLine2":this.state.addressLine2,"phone_number":this.state.phone_number,
                    "shippingCityName":this.state.shippingCityName,"pin_code":this.state.pin_code,
                    "notification_email":this.state.notification_email,
                    "notification_phone":this.state.notification_phone,"instructions":this.state.instructions};
        
        cookie.save('checkoutdata', data);
       
        console.log(" checkoutdata cookies = ", cookie.load('checkoutdata'));
        this.setState({
          firsttest : true,
          middlesection : true,
          middletest : false
        }) 
    } else {
      console.log("fields are required");
      if(!this.state.shippingCityName) {
        $("div[name=shippingCityName]").remove();
        $("input[placeholder=City]").after("<div name='shippingCityName' class='error'>City is required.</div>")
      }
    }
  } 
  useDifferentShipping () {
    this.setState({

      selection : "default",
      hidesection : "firstsection",
      firsttest : false,
      middletest: true,
      newShippingAddress: true
    })
  }
  editAddress() {

     this.setState({
      selection : "default",
      hidesection : "firstsection",
      firsttest : false,
      middletest: true,
      newShippingAddress: false
    })
  }
  addNewShipping () {
    this.setState({
      newShippingAddress : false,
      useAddress : null,
      name: '',
      last_name : '',
      phone_number : '',
      addressLine1 : '',
      addressLine2 : '',
      shippingCityName : '',
      pin_code : '',
      notification_email : '',
    })
  }

  sameBillingAddress () {
    if ($('input:checkbox[name=same_billing_address]:checked').val()) {
        

        var biller_name = this.state.name;
        var biller_last_name = this.state.last_name;
        var biller_phone_number = this.state.phone_number;
        var biller_addressLine1 = this.state.addressLine1;
        var biller_addressLine2 = this.state.addressLine2; 
        var biller_city = this.state.shippingCityName;
        var biller_pin_code = this.state.pin_code;

        this.setState({
           same_billing_address : true, 
           biller_name : this.state.name,
           biller_last_name : this.state.last_name,
           biller_phone_number : this.state.phone_number,
           biller_addressLine1 : this.state.addressLine1,
           biller_addressLine2 : this.state.addressLine2,
           biller_city : this.state.shippingCityName,
           biller_pin_code : this.state.pin_code,
        }) 
       
              
    } else {
       this.setState({
           same_billing_address : false,
           biller_name : '',
           biller_last_name : '',
           biller_phone_number : '',
           biller_addressLine1 : '',
           biller_addressLine2 : '',
           biller_city : '',
           biller_pin_code : '',
       })
    }
  }
  cancelCard () {
    $('.error').remove();
    this.setState({
      already_cardnumber : true,
      card_selected : true,
      change_card : true,
    })
  }
  editPayment (event) {
    event.preventDefault();
    
    this.setState({
      middlesection : true,
      middletest : false,
      firsttest : true,
    })
  }
  getCity (query) {
    var self = this;
    API.get(baseAPIUrl + "cities?query=" + query, null, function(err,result,more) {
      console.log("<-----get Cart ------> ", err, result, more);
      self.setState({
        cities : result
      });
    });
  }

  payment_continue (event) {
      $('.error').remove();
      event.preventDefault();
      if((typeof this.state.change_card)=="string") {
        var change_card = this.state.change_card;
        var masked_number = this.state.change.masked_number
        this.setState({
          change : JSON.parse(change_card), 
          card_number : "XXXX XXXX XXXX " + masked_number
        })
      }
      var check = true; 
      isNotEmpty("biller_name",this.state.biller_name) ? null : (check = false) 
      isNotEmpty("biller_last_name",this.state.biller_last_name) ? null : (check = false)
      isNotEmpty("biller_phone_number",this.state.biller_phone_number) && isPhoneNumber("biller_phone_number",this.state.biller_phone_number) ? null : (check = false)
      isNotEmpty("biller_addressLine1",this.state.biller_addressLine1) ? null : (check = false)
      isNotEmpty("biller_pin_code",this.state.biller_pin_code) ? null : (check = false)
      isNotEmpty("card_number",this.state.card_number) && isNumber("card_number",this.state.card_number) ? null : (check = false)
      !this.state.card_selected && isNotEmpty("card_cvv",this.state.card_cvv) ? null : (check = false)
      !this.state.card_selected && isSelected("expiry_month",this.state.expiry_month) ? null : (check = false)
      !this.state.card_selected && isSelected("expiry_year",this.state.expiry_year) ? null : (check = false)
      
      if ((this.state.same_billing_address || (check)) && this.state.card_number && ((!this.state.card_selected && this.state.card_cvv && this.state.expiry_month && this.state.expiry_year) || this.state.already_cardnumber || this.state.change_card)) {        
        var data = {"biller_name":this.state.biller_name,"biller_last_name":this.state.biller_last_name,"biller_phone_number": this.state.biller_phone_number,
                      "biller_company_name":this.state.biller_company_name,"biller_addressLine1":this.state.biller_addressLine1,
                      "biller_addressLine2":this.state.biller_addressLine2,"biller_city":this.state.biller_city,
                      "biller_pin_code":this.state.biller_pin_code,
                      "same_billing_address": this.state.same_billing_address};
        cookie.save('checkoutbillingdata', data);
        this.setState({
          showsection :  "lastsection",
          lastsection :  true,
          middletest  : true      
        })
      } else { 

      console.log("fields are required");
      
      if(!this.state.biller_city) {
        $("div[name=biller_city]").remove();
        $("input[placeholder='Biller City']").after("<div name='biller_city' class='error'>City is required.</div>")
      }
    } 
  }
  PlaceOrder () {
    const { dispatch } = this.props;
    var self = this;
    var shipping_address = {};
    shipping_address.first_name = this.state.name;
    shipping_address.last_name = this.state.last_name;
    shipping_address.address_line1 = this.state.addressLine1;
    shipping_address.address_line2 = this.state.addressLine2;
    shipping_address.city = this.state.shippingCityName;
    shipping_address.stateor_province = this.state.stateorprovince;
    shipping_address.phoneno = this.state.phone_number;
    shipping_address.postalcode = this.state.pin_code; 
    shipping_address.currentAddressId = this.state.currentAddressId;
    var cart_data = this.state.cart_data;
    cart_data.shipping_address = shipping_address;
    this.setState({
      shipping_address : shipping_address,
      cart_data : cart_data,
      place_order : true
    })
    if (!this.state.same_billing_address) {  
      var billing_address = {};
      billing_address.address_line1 = this.state.biller_addressLine1;
      billing_address.address_line2 = this.state.biller_addressLine2;
      billing_address.phoneno = this.state.biller_phone_number;
      billing_address.city = this.state.biller_city;
      // $scope.billing_address.stateor_province = $scope.billingStateorprovince;
      billing_address.postalcode = this.state.biller_pin_code;
      cart_data.billing_address = billing_address;
      this.setState({
        billing_address : billing_address,
        cart_data : cart_data,
      })
    } else {
      var billing_address = {};
      billing_address.address_line1 = shipping_address.address_line1;
      billing_address.address_line2 = shipping_address.address_line2;
      billing_address.phoneno = shipping_address.phoneno;
      billing_address.city = shipping_address.city;
     
      billing_address.postalcode = shipping_address.postalcode;
      cart_data.billing_address = billing_address;
      this.setState({
        billing_address : billing_address,
        cart_data : cart_data,
      })     
    }
    var cartObj = {
      item: this.state.cart_data.items[0],
      shipping_address: this.state.cart_data.shipping_address,
      billing_address : this.state.cart_data.billing_address,
      saveShippingAddress: this.state.saveShippingAddress,
      use_address: this.state.use_address
    }
    dispatch(qtyChangedAPI(cartObj));
    self.setState({
        token: null,
        paymentErrorMessage : null,
    });
    if(!self.state.card_selected) {
      Stripe.card.createToken({"number":self.state.card_number,"cvc":self.state.card_cvv,"exp-month":self.state.expiry_month,"exp-year":self.state.expiry_year}, function (status, response) {
        if (response.error) {
          self.setState({
            paymentErrorMessage : response.error.message,
            place_order : false
          })
        } else {
          // response contains id and card, which contains additional card details
          console.log("token ::::::", response.id);
          self.setState({
            token : response.id
          })
          
          let obj = {
            token_id: self.state.token,
            save_card: self.state.saveCardDetails,
            card_selected: self.state.card_selected,
            amount: self.state.total,
            card_number: self.state.card_number,
            card_name: self.state.card_name,
            change_card: self.state.change_card,
            payer_id: self.state.name_on_card
          }
          API.post(baseAPIUrl + "payment", obj, function(err,result,more) {
            console.log("<-----updateCart Cart ------>");
            console.log("Error:", err);
            console.log("Result:", result);
            console.log("Details:", more);
            console.log("<-----updateCart Cart ------>");

            if(err || result === 'error' || more && more.responseText.indexOf('error') > -1)  {
              // $rootScope.$emit('cartDelete', data);
              console.log("error in payment",err);
              self.setState({
                paymentErrorMessage : err,
                place_order : false
              })
              // $('#panel1v .error-message').html(err);
            }
            else {
              console.log("payment success",result);
              cookie.remove('checkoutbillingdata');
              cookie.remove('checkoutdata');
              // delete $cookies['checkoutbillingdata'];
              // delete $cookies['checkoutdata'];
              // $rootScope.$emit('cartDelete', result);
              // $state.go('thankyou',{orderid:result.ordernumber});
              self.props.history.push("thankyou/"+result.ordernumber);
            }  
          });
        }
      });
    } else {  // if card is selected then use saved card customer id in DB
      // console.log("--card is selected--------",$scope.total);
      let obj = {
        token_id: this.state.token,
        save_card: this.state.saveCardDetails,
        card_selected: this.state.card_selected,
        amount: this.state.total,
        card_number: this.state.card_number,
        card_name: this.state.name_on_card,
        change_card: this.state.change_card
      }
      API.post(baseAPIUrl + "payment", obj, function(err,result,more) {
        if(err || result === 'error' || more && more.responseText.indexOf('error') > -1)  {
          // $rootScope.$emit('cartDelete', data);
          console.log("error in payment",err);
          self.setState({
            paymentErrorMessage : err,
            place_order : false
          })
          // $('#panel1v .error-message').html(err);
        }
        else {
          console.log("payment success",result);
          cookie.remove('checkoutbillingdata');
          cookie.remove('checkoutdata');
          self.props.history.push("thankyou/"+result.ordernumber);
        }
      });
    }
  } 
  addNewCard (event) {
    event.preventDefault();
    this.setState({
      already_cardnumber : false,
      card_selected : false,
      change_card : false,
      card_number : '',
    })

  }
  changeCard (event) {
    var change_card = _.filter(this.state.user_payment,function(change_card){
      return change_card._id == event.target.value;
    })
    
    this.setState({
      change_card : change_card[0]
    })
  }
  changeAddress (event) {

    var useAddress = _.filter(this.state.useAddress,function(change_card){
      return useAddress._id == event.target.value;
    })
    
    this.setState({
      useAddress : useAddress[0]
    })
  }
  render() {
 
    var self = this;
    var host = config.baseApiUrl;
    return (
      <div>
        <div  className="col-xs-12 none" id="checkout" className="container">
          <div className="col-xs-12 padding_lg text-center f5">Shipping Information</div>
          <div>
            {this.state.firstsection ? (
              <div>
                <form name="myForm" noValidate>
                  {!this.state.firsttest ? (
                    <section>
                      <div className="col-xs-12 border_b padding_md">
                        <div className="step text-center f2">1</div>
                        <div className="step_name f7">Shipping</div>
                      </div>
                      <div className="col-xs-12 padding_md">
                        {this.state.newShippingAddress ? (
                          <div className="col-md-6 col-sm-7 col-xs-12">
                            <div className="col-xs-12 none border margin_md">
                              <div className="col-xs-12 bg_1 padding_sm f7">Shipping Address</div>
                              <div className="col-xs-12 padding_sm more">
                                {this.state.user_checkout.shippinglocations ? this.state.user_checkout.shippinglocations.map(function(useAddress,index){
                                  return (
                                    <div key={index} className="col-xs-12 none padding_md phone"> 
                                      <div className="col-xs-12 none">
                                        <input name='useAddress' className="pull-left" type="radio" value={useAddress._id} onChange={(event)=> this.changeAddress(event)} checked={useAddress._id == self.state.useAddress._id} />
                                        <div className="pull-left">
                                          <div>{useAddress.first_name +" "+useAddress.last_name}</div>
                                          <div>{useAddress.phoneno}</div>
                                          <div>{useAddress.address_line1}</div>
                                          <div>{useAddress.address_line2}</div>
                                          <div>{useAddress.city}</div>
                                          <div>{useAddress.postalcode}</div>
                                        </div>
                                      </div>
                                    </div>
                                  )
                                }) : null}
                              </div>
                              <div className="col-xs-12 phone" onClick={() => this.addNewShipping()}><a >add_a_new_contact_or_shipping_address</a></div>
                            </div>    
                          </div>
                        ) : (
                          <div className="col-md-6 col-sm-7 col-xs-12">
                            <div className="col-xs-12 none border margin_md">
                               <div className="col-xs-12 bg_1 padding_sm f7">Shipping Address</div>
                               <div className="col-xs-12 padding_sm">
                                  <div className="col-md-7 col-sm-7 col-xs-6 padding_sm">
                                    <input type="text" name="name" onChange={(event) => {this.setStateValue(event, 'name')}} placeholder="First Name" value={this.state.name} required />
                                                     
                                  </div>
                                  <div className="col-md-5 col-sm-5 col-xs-6 padding_sm">
                                    <input type="text" name="last_name" onChange={(event) => {this.setStateValue(event, 'last_name')}} placeholder="Last Name" value={this.state.last_name} required />
                                     
                                  </div>
                                   
                               <div className="col-xs-12 padding_sm">
                                  <input type="text" name="company_name" onChange={(event) => {this.setStateValue(event, 'company_name')}} placeholder="Company Name" value={this.state.company_name} />               
                               </div>
                               <div className="col-xs-12 padding_sm">
                                  <input type="text" value={this.state.phone_number} onChange={(event) => {this.setStateValue(event, 'phone_number')}} pattern="/^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/" name="phone_number" placeholder="Phone" required />
                                                                 
                               </div>
                               <div className="col-xs-12 padding_sm">
                                  <input type="text" name="addressLine1" value={this.state.addressLine1} onChange={(event) => {this.setStateValue(event, 'addressLine1')}} placeholder="Street Address" required />                      
                               </div>
                               
                               <div className="col-xs-12 padding_sm">
                                  <input type="text" name="addressLine2" value={this.state.address_line2}  onChange={(event) => {this.setStateValue(event, 'addressLine2')}} placeholder="Apt, Suite" />           
                               </div>
                               <div className="col-xs-12 padding_sm search_all citySearch">
                                  <Typeahead name="shippingCityName" placeholder="City" value={this.state.shippingCityName} options={this.state.cities} maxVisible={10} onChange={(event) => {this.getCity(event.target.value)}}  onOptionSelected = {(val) => {this.setCityValue(val, 'shippingCityName')}}/>                  
                               </div>
                        
                                  <div className="col-md-6 col-sm-5 col-xs-12 padding_sm">
                                     <input type="text" value={this.state.pin_code} onChange={(event) => {this.setStateValue(event, 'pin_code')}} name="pin_code" pattern="/^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/" placeholder="Postal Code" required />
                                                     
                                  </div>                   
                                  <div className="col-md-6 col-sm-5 col-xs-12 padding_sm">
                                     <input type="checkbox" name="special_instruction_check" onChange={(event) => {this.setStateValue(event, 'special_instruction_check')}}  />Special Instructions ?
                                  </div>
                                  {this.state.special_instruction_check ? (
                                    <div className="col-xs-12 padding_sm">
                                       <input type="text" name="instructions"  value={this.state.instructions} onChange={(event) => {this.setStateValue(event, 'instructions')}} placeholder="Instructions" required />                     
                                    </div>                   
                                    ) : null}
                               </div>
                               <div className="col-xs-12 bg_1 padding_sm f7">Shipping Notifications</div>
                               <div className="col-xs-12 padding_sm">
                                  <input type="email" value={this.state.notification_email} name="notification_email" onChange={(event) => {this.setStateValue(event, 'notification_email')}} placeholder="Email Address" required /> 
                                               
                               </div>                
                               <div className="col-xs-12 padding_sm">
                                  <input type="text" value={this.state.notification_phone} onChange={(event) => {this.setStateValue(event, 'notification_phone')}} pattern="/^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/" name="notification_phone" placeholder="Phone" /> 
                                              
                               </div> 
                            </div>
                          </div>
                        )} 
                        <div className="col-md-6 col-sm-5 col-xs-12">
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Shipping Details</div>
                            <div className="col-xs-12 padding_sm f10">{config.shipment_message}</div>
                          </div>
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Shipping Notifications</div>
                            <div className="col-xs-12 padding_sm f10">{config.notification_message}</div>
                          </div>
                        </div>
                        <div className="col-xs-12 text-right"><button  type="button" onClick={() => this.shipping_address()}  className="btn_1 text_2 border ">Continue</button></div>
                      </div>   
                    </section>
                  ) : (
                    <section>
                      <div className="col-xs-12 border_b padding_md">
                        <div className="step text-center f2 step_active">1</div>
                        <div className="step_name f7">Shipping Information</div>
                      </div>
                      <div className="col-xs-12 padding_md">
                        <div className="col-md-6 col-sm-7 col-xs-12">
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Shipping Address</div>
                            <div className="col-xs-12">{this.state.name+" "+this.state.last_name}</div>
                            <div className="col-xs-12">{this.state.phone_number}</div>
                            <div className="col-xs-12">{this.state.addressLine1}</div>
                            {this.state.addressLine2 ? (<div className="col-xs-12">{this.state.addressLine2}</div>) : null}
                            <div className="col-xs-12">{this.state.shippingCityName}</div>
                            <div className="col-xs-12">{this.state.pin_code}</div>
                            {this.state.instructions ? (<div className="col-xs-12">{this.state.instructions}</div>) : null }
                            {this.state.multipleShippingAddress ? (<div className="col-xs-12" onClick={() => this.useDifferentShipping()}><a >Use a different contact or shipping address</a></div>) : null }
                          </div>
                        </div>
                        <div className="col-md-6 col-sm-5 col-xs-12">
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Shipping Details</div>
                            <div className="col-xs-12 padding_sm f10">{config.shipment_message}</div>
                          </div>
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Shipping Notifications</div>
                            <div className="col-xs-12 padding_sm f10">{config.notification_message}</div>
                          </div>
                        </div>
                        <div className="col-xs-12 text-right"><button type='button' onClick={() => this.editAddress()}>Edit</button></div>
                      </div>
                    </section>
                  )} 
                </form>
              </div> 
            ) : null}
            <form name="middleForm" noValidate>
              { this.state.middlesection ? (
                <div>
                  {!this.state.middletest ? (
                    <section>
                      <div className="col-xs-12 padding_md border_b">
                        <div className="step text-center f2">2</div>
                        <div className="step_name f7">Payment</div>
                      </div>
                      <div className="col-xs-12 padding_md">            
                        <div className="col-md-6 col-sm-7 col-xs-12">
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Billing Address</div>
                            <div className="col-xs-12 padding_sm">
                              <input type="checkbox" name="same_billing_address" onChange={() => this.sameBillingAddress('same_billing_address')} checked={this.state.same_billing_address} />Same as Shipping Information
                            </div>
                            {!this.state.same_billing_address ? (
                              <div className="col-xs-12 padding_sm">
                                <div className="col-md-7 col-sm-7 col-xs-6 padding_sm">
                                  <input type="text" name="biller_name" onChange={(event) => {this.setStateValue(event, 'biller_name')}} value={this.state.biller_name} placeholder="First Name" required disabled={this.state.same_billing_address} />
                                </div>
                                <div className="col-md-5 col-sm-5 col-xs-6 padding_sm">
                                  <input type="text" name="biller_last_name" onChange={(event) => {this.setStateValue(event, 'biller_last_name')}} value={this.state.biller_last_name} placeholder="Last Name" required /> 
                                </div>
                                <div className="col-xs-12 padding_sm">
                                  <input type="text" name="" value={this.state.biller_company_name}  onChange={(event) => {this.setStateValue(event, 'biller_company_name')}} placeholder="Company Name" />               
                                </div>
                                <div className="col-xs-12 padding_sm">
                                  <input type="text" pattern="/^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/" name="biller_phone_number" onChange={(event) => {this.setStateValue(event, 'biller_phone_number')}} placeholder="Phone" value={this.state.biller_phone_number}  required />
                                </div>
                                <div className="col-xs-12 padding_sm">
                                  <input type="text" value={this.state.biller_addressLine1}  name="biller_addressLine1" onChange={(event) => {this.setStateValue(event, 'biller_addressLine1')}} placeholder="Street_address" required />
                                </div>
                                <div className="col-xs-12 padding_sm">
                                  <input type="text" onChange={(event) => {this.setStateValue(event, 'biller_addressLine2')}} name="" value={this.state.biller_addressLine2}  placeholder="Apt suite" />              
                                </div>
                                <div className="col-xs-12 padding_sm search_all">
                                  <Typeahead name="city" placeholder="Biller City" value={this.state.biller_city}  options={this.state.cities} maxVisible={10} onChange={(event) => {this.getCity(event.target.value)}}  onOptionSelected = {(val) => {this.setCityValue(val, 'biller_city')}}/>                                                                            
                                </div>
                                <div className="col-md-6 col-sm-5 col-xs-12 padding_sm">
                                  <input type="text" onChange={(event) => {this.setStateValue(event, 'biller_pin_code')}} name="biller_pin_code" value={this.state.biller_pin_code}  placeholder="Postal code" pattern="/^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/" required />
                                </div>
                              </div>
                            ) : (
                              <div className="col-xs-12 none">           
                                <div className="col-xs-12">{this.state.biller_name || this.state.name} {this.state.biller_last_name || this.state.last_name}</div>
                                <div className="col-xs-12">{this.state.biller_phone_number || this.state.phone_number}</div>
                                <div className="col-xs-12">{this.state.biller_addressLine1 || this.state.addressLine1}</div>
                                <div className="col-xs-12">{this.state.biller_addressLine2 || this.state.addressLine2}</div>
                                <div className="col-xs-12">{this.state.biller_city || this.state.shippingCityName}</div>
                                <div className="col-xs-12">{this.state.biller_pin_code || this.state.pin_code}</div>
                                {this.state.instructions ? (<div className="col-xs-12">{this.state.instructions}</div>) : null }
                              </div>
                            )} 
                          </div>
                        </div>
                        {!this.state.already_cardnumber ? (
                          <div className="col-md-6 col-sm-5 col-xs-12">
                            <div className="col-xs-12 none border margin_md">
                              <div className="col-xs-12 bg_1 padding_sm f7">Payment Details</div>
                              <div className="col-xs-12 padding_sm">
                                <div className="col-xs-12 padding_sm"><img src="image/card.png" /></div>
                                <div className="col-xs-12 padding_sm">
                                  <input type="number" name="card_number" value={this.state.card_number} onChange={(event) => {this.setStateValue(event, 'card_number')}} placeholder="Card Number" maxLength="16" required />
                                </div>
                                <div id="mylistID" ></div>
                                <div className="col-xs-12 padding_sm">
                                  <input type="text" name="name_on_card" value={this.state.name_on_card} onChange={(event) => {this.setStateValue(event, 'name_on_card')}} placeholder="Name on card" required />               
                                </div>
                                <div className="col-xs-6 padding_sm">
                                  <select name="expiry_month" onChange={(event) => {this.setStateValue(event, 'expiry_month')}} defaultValue={this.state.expiry_month} required >
                                    <option value="" >MM</option>
                                    { config.Month_Array.map(month =>  
                                        <option key={month} value={month} >{month}</option>
                                    )} 
                                  </select>  
                                </div>
                                <div className="col-xs-6 padding_sm">
                                  <select name="expiry_year" onChange={(event) => {this.setStateValue(event, 'expiry_year')}} defaultValue={this.state.expiry_year} required >
                                    <option value=""  >YYYY</option>
                                    { config.Year_Array.map(function(year){
                                      return (
                                        <option key={year} value={year}  >{year}</option>
                                      )
                                    } )}
                                  </select>   
                                </div>
                                <div className="padding_sm pull-left">
                                  <input className="input_sm" type="number" name="card_cvv" value={this.state.card_cvv} onChange ={(event) => {this.setStateValue(event, 'card_cvv')}} placeholder="CVV" maxLength="4" required  minLength="3" />
                                </div>
                                <div className="padding_sm pull-left"> 
                                  <input type="checkbox" className="padding_sm" name="saveCardDetails" onChange={(event) => {this.setStateValue(event, 'saveCardDetails')}} />Save Card Details
                                </div>
                                {this.state.user_payment ? (
                                  <div className="col-xs-12 padding_sm" onClick={() => this.cancelCard()}><a >Cancel</a></div>
                                ) : null }
                                <div className="col-xs-12 padding_sm f10"> 
                                  {config.payment_message}
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : (
                          <div className="col-md-6 col-sm-5 col-xs-12" >
                            <div className="col-xs-12 none border margin_md">
                              <div className="col-xs-12 bg_1 padding_sm f7">Payment Details</div>
                              <div className="col-xs-12 none more">
                                {this.state.user_payment ? (
                                  this.state.user_payment.map(function(change_card ,index) { 
                                    return (
                                      <div key={index} className="col-xs-12 ccard padding_md" >
                                        <input name="card_selected" className="pull-left" type="radio" value={change_card._id} checked={self.state.change_card._id == change_card._id} onChange={(event) => self.changeCard(event)}/> 
                                        <img src="image/visa.png" /> XXXX XXXX XXXX {change_card.masked_number}
                                      </div>
                                    )
                                  })
                                ) : null }  
                              </div>
                              <div className="col-xs-12 padding_sm" onClick={(event) => this.addNewCard(event)}><button className="btn_3 text_4 border">Add New Card</button></div>
                            </div>
                          </div>
                        )}
                        <div className="col-xs-12 text-right">
                          <button  className="btn_1 text_2 border" id="payment_continue" onClick={(event) => this.payment_continue(event)}>Continue</button>
                        </div>
                      </div>
                    </section>
                  ) : (
                    <section>
                      <div className="col-xs-12 padding_md border_b">
                        <div className="step text-center f2 step_active">2</div>
                        <div className="step_name f7">Payment</div>
                      </div>
                      <div className="col-xs-12 padding_md">
                        <div className="col-md-6 col-sm-7 col-xs-12">
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Billing Address</div>
                            <div className="col-xs-12">{this.state.biller_name || this.state.name} {this.state.biller_last_name || this.state.last_name}</div>
                            <div className="col-xs-12">{this.state.biller_phone_number || this.state.phone_number}</div>
                            <div className="col-xs-12">{this.state.biller_addressLine1 || this.state.addressLine1}</div>
                            <div className="col-xs-12">{this.state.biller_addressLine2 || this.state.addressLine2}</div>
                            <div className="col-xs-12">{this.state.biller_city || this.state.shippingCityName}</div>
                            <div className="col-xs-12">{this.state.biller_pin_code || this.state.pin_code}</div>
                            {this.state.instructions ? (<div className="col-xs-12">{this.state.instructions}</div>) : null }
                          </div>
                        </div>
                        <div className="col-md-6 col-sm-5 col-xs-12">
                          <div className="col-xs-12 none border margin_md">
                            <div className="col-xs-12 bg_1 padding_sm f7">Payment Details</div>
                            <div className="col-xs-12 padding_sm"><img src="image/card.png"/></div>
                            <div className="col-xs-12 padding_sm">{this.state.name_on_card}</div>
                            <div className="col-xs-12 padding_sm">{this.state.card_number}</div>
                            {this.state.user_payment ? (<div className="col-xs-12 padding_sm" onClick={(event) => this.editPayment(event)}>
                                <a >Use a different credit card</a>
                              </div>
                            ) : null}
                          </div>
                        </div>
                        <div className="col-xs-12 text-right">
                          <button onClick={(event) => this.editPayment(event)}>Edit</button>
                        </div>
                      </div>
                    </section>
                  )}
                </div>
              ) : null} 
            </form>
            {this.state.lastsection ? (
              <div>
                {!this.state.lasttest ? (
                  <section>
                    <div className="col-xs-12 padding_md">
                      <div className="col-xs-12 padding_md border_b">
                        <div className="step text-center f2">3</div>
                        <div className="step_name f7">Confirm Order</div>
                      </div>
                      <div className="col-xs-12 padding_md f7"><b>Product Cart</b></div>        
                      <div className="col-xs-12 padding_lg border_b">
                        <div className="col-md-12 col-sm-12 col-xs-12 none pull-right b_none">
                          {this.state.cart && this.state.cart.cart_data.items[0] ? ( this.state.cart.cart_data.items[0].store_items.map(function (item,index) { 
                            return (
                              <div key={item._id}>
                                <div id={"viewcart"+0} className="col-xs-12 border_b padding_md">
                                  <div className="col-md-2 col-sm-4 col-xs-4 none text-center buy_img phone">
                                    <a href="productdetail({productid:item.item_code})"><img src={host + item.item_image}/></a>
                                  </div>
                                  <div className="col-md-10 col-sm-8 col-xs-8 none phone">
                                    <div className="col-xs-12 none">
                                      <a className="col-xs-12 none z_black limit f7" href="productdetail({productid:item.item_code})">{item.name}</a>
                                    </div>
                                    <div className="col-xs-12 none limt f8">
                                      {item.description}
                                    </div>
                                    {item.variant_type=='P' ? (
                                      <div className="col-xs-12 none limit">
                                        <b>{"package"}</b> : {item.variation_name}
                                      </div>
                                    ) : ''}
                                    { item.attributes.length > 0 ? (<Attributes data={item.attributes} />) : null}
                                    <div className="col-xs-12 none">
                                      <div className="col-xs-5 none limit text_1 f8">{item.product_price + config.currency}</div>
                                      <div className="col-xs-2 none"> {"Qty "+item.quantity_ordered}</div>
                                      <div className="col-xs-5 none text-right limit f7">{item.payed_price + config.currency}</div>
                                    </div>
                                  </div>
                                </div>
                                {self.state.cart.cart_data.items[0].store_code== self.state.coupon.viewfix_store_code ? (
                                  <div className='col-xs-12 f6 text_4 text-right' >
                                    <del>Coupon {self.state.coupon.invalid_code}</del> invalid <i className='coupon_cancel glyphicon glyphicon-remove error'></i><br></br>
                                    <button className="btn_3 f8 text_4 border padding_md" ng-click="showOrderFix()">click to view/fix</button>
                                  </div> 
                                ) : null}
                                {self.state.cart.cart_data.items[0].coupon_discount ? (
                                  <div className='col-xs-12 f6 text_4 text-right' >
                                    Coupon {self.state.cart.cart_data.items[0].coupon_code} applied  (discount {self.state.cart.cart_data.items[0].coupon_discount + config.currency})<i className='glyphicon glyphicon-ok text_1'></i>
                                  </div> 
                                ) : null}
                              </div>     
                            )})
                          ) : null}     
                        </div>
                        <div className="col-md-6 col-sm-6 col-xs-12 padding_md pull-right f8">
                          <div className="col-md-8 col-sm-6 col-xs-8 text-right none">
                            <div>Subtotal:  </div>
                            <div>Shipping: </div>
                            <div>Taxes: </div>
                            {this.state.cart && this.state.cart.total_coupon_discount ? (
                              <div>Discount: </div>
                            ) : null}
                          <div className="z_black"><b>Total:</b></div>
                        </div>
                        {this.state.cart ? (
                          <div className="col-md-4 col-sm-6 col-xs-4 text-right none">
                            <div><b>{config.toFixed(this.state.cart.subtotal) + config.currency}</b></div>
                            <div>{config.toFixed(this.state.cart.shippingCost) + config.currency}</div>
                            <div>{config.toFixed(this.state.cart.vat + this.state.cart.sales_tax) + config.currency }</div>
                            {this.state.cart.total_coupon_discount ? (
                              <div >{config.toFixed(this.state.cart.total_coupon_discount) + config.currency }</div>
                            ) : null}
                            <div><b>{ config.toFixed(this.state.cart.total) + config.currency }</b></div>
                          </div>
                        ) : null}
                        <div className="col-md-8 col-sm-6 col-xs-12 pull-right text-right padding_md"><button className="btn_1 text_2 border" id="PlaceOrder" onClick={() => this.PlaceOrder()} disabled={this.state.place_order}>Place Order</button></div>
                          {this.state.place_order ? (
                            <div className="col-xs-12 checkout_loading" ><img src="image/loadingimg.gif"/></div>
                          ) : null}
                          <div className="col-xs-12 text-right error none"><span>{self.state.paymentErrorMessage}</span></div>
                        </div>
                      </div>
                    </div>
                  </section>
                ) : null }
              </div> 
            ) : null}
          </div> 
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let cart = (state.cartReducer && Object.keys(state.cartReducer).length) ? state.cartReducer.cart : {};
  return {state: state, cart: cart}
}

Checkout = connect(mapStateToProps)(Checkout)
export default Checkout;

class Attributes extends Component { 

  render() {  
    var attributes = this.props.data.map(function(attribute){      
        return (
              <div className="col-xs-12 none f11" >
                <b>{attribute.name}</b> : {attribute.value}
              </div>   
        )        
    })
    return (
      <div> 
        {attributes} 
      </div>
      )
  }
}