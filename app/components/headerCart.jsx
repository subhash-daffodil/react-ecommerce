import React, { Component } from 'react';
import { Link } from 'react-router';
import config from "../common/config.jsx";
import store from '../store.jsx';

let host = config.baseApiUrl; 

export default class HeaderCart extends Component {

	constructor(props) {
  	super(props);
  	this.state = {
  		showCart: false
  	}
	}

	showCart () {
   	this.setState({
      showCart : true
   	});
  }

  hideCart () {
   	this.setState({
      showCart : false
   	});
  }

  userStatus() {  
    var self = this;
    var user = store.getState().userReducer.authUser;
    if(user && typeof(user) === "string") {
      user = JSON.parse(user);
    }
    if(user && Object.keys(user).length) {
      self.context.router.push("/checkout");
    } else {
      self.props.onOpenCloseDialog(true);
    }
  }

	render() {
		let cart_data = this.props.cart_data;
    let quantity_ordered = 0;  
		let cart;
    let items;

		if(cart_data && cart_data.items && cart_data.items[0]) {
      cart_data.items[0].store_items.map(function (cart) { 
        quantity_ordered = parseInt(quantity_ordered) + parseInt(cart.quantity_ordered);
      });
    }
		if(this.state.showCart) {
      if(cart_data && Object.keys(cart_data).length && cart_data.items[0]) {
        items = cart_data.items[0].store_items.map(function (cart, index) {
          return (
            <div key={"cart_" + index}  className="col-xs-12 none">
              <div className="col-md-3 col-sm-4 col-xs-4 none">
                <Link to={"product/"+cart.item_code}> 
                	<img src={host + cart.item_image} />
                </Link>
              </div>
              <div className="col-md-9 col-sm-8 col-xs-8 padding_sm">
                <div className="col-xs-7 none">
                  <Link className="text_3" to={"product/"+cart.item_code}>{cart.name.length > 10 ? cart.name.substring(0,10)+"..." : cart.name}</Link>
                </div>
                <div className="col-xs-5 none text-right">{config.currency+config.toFixed(cart.payed_price)}</div>
                {cart.variant_type=='P' ? (
                  <div className="col-xs-12 none f11" >
                    <b>{"package"}</b> : {cart.variation_name}
                  </div>
                ) : ''}
                { cart.attributes.length > 0 ? (<Attributes data={cart.attributes} />) : null}
                
                <div className="col-xs-12 none">{"Qty " +cart.quantity_ordered}</div>
              </div>
            </div>
          )   
        }) 

        cart = (
          <div className="popup_1 col-xs-12 pull-right padding_sm" id="cart_popup" onMouseEnter={this.showCart.bind(this)} onMouseLeave={this.hideCart.bind(this)}>
            {items}
            <div className="col-xs-12 none text-left "><h4>{ "SUBTOTAL $"+ config.toFixed(cart_data.subtotal) }</h4></div>
            <div className="col-xs-12 none">
              <div className="col-xs-4 text-left none margin_sm">
                <Link to="viewCart">
                  <button type='button' className="btn btn-primary">View Cart</button>
                </Link>
              </div>
              <div className="col-xs-8 text-right none margin_sm">
                <button className="btn_1 text_2 border" onClick={() => this.userStatus()}>
                  Proceed to Checkout
                </button>
              </div>
            </div>
          </div>
        )
      }  else {
        cart = ( 
          <div className="popup_1 col-xs-12 pull-right padding_sm" id="cart_popup" onMouseEnter={this.showCart.bind(this)} onMouseLeave={this.hideCart.bind(this)}>
            <div className="col-xs-12 text-center none">Your cart is empty.</div>
          </div>
        )
      }
    }
		return (
			<div>
				<div className="pull-right none" onMouseEnter={this.showCart.bind(this)} onMouseLeave={this.hideCart.bind(this)}>
	        <button className="btn_3">
	          <i className="glyphicon glyphicon-shopping-cart f3 text_1"></i>
	          {cart_data && cart_data.items && cart_data.items[0] && cart_data.items[0].store_items[0] && cart_data.items[0].store_items.length > 0  ? (
	            <div  className="cart_value text-center f11 ng-binding ng-scope">{quantity_ordered}</div>
	          ) : ''}
	        </button>
	      </div>
	      {cart}
	    </div>
		)
	}
}

HeaderCart.contextTypes = {
  router: React.PropTypes.object.isRequired
};