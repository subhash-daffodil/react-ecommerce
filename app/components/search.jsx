import React, { Component } from 'react';
import { Link } from 'react-router';
import config from "../common/config.jsx";
import API from "../common/api.jsx";

let baseAPIUrl = config.baseApiUrl;

var scroll_in_process = false;
var lastScrollTop = 0;
var skip = 0;
var limit = 6;
var listener;

export default class Search extends Component{
  constructor(props) {
    super(props);
    this.state = {
      productArray : null,
    };
  }

  componentWillMount() {
    var self = this;
    this.setState({
        query: this.props.params.query
      },function() {
        listener = self.handleScroll.bind(self)
        window.addEventListener('scroll', listener);
        self.searchData()
      })
  }
  componentWillReceiveProps(route) {
    var self = this;
    if(route.params.query != this.state.query) {
      this.setState({
        query: route.params.query
      },function() {
        self.searchData();
      }) 
    }
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', listener);
  }
  searchData() {
    var self = this;
    var query = this.state.query;
    API.get(baseAPIUrl + "search?query="+query+"&limit=6&skip=0", null, function(err,result,more) {
      
      self.setState({
        query : query,
        productArray : result
      })
    });
    scroll_in_process = false;
    lastScrollTop = 0;
    skip = 6;
    limit = 6;
  }

  handleScroll(event) {
    var self = this;
    var st = $(window).scrollTop();
    if (st > lastScrollTop) {
      if (!scroll_in_process && $("#scrollDiv").offset() && (st + $(window).height()) > $("#scrollDiv").offset().top) {
        scroll_in_process = true;
        API.get(baseAPIUrl + "search?query="+this.state.query+"&limit="+limit+"&skip="+skip, null, function(err,result,more) {
          if(data.length == 0) {
            console.log("Data End!!");
          } else {
            var productArray = self.state.productArray;
            for(var i = 0; i < data.length; i++) {
             productArray.push(data[i]);
            }
            self.setState({
              productArray : productArray
            })
            skip = skip + limit;
            scroll_in_process = false;
          }
        }); 
      }
    }
    lastScrollTop = st;
  }
  

  render() {  
    var host = config.baseApiUrl; 
    var client = config.clientUrl; 
    var products = null;  
    if(this.state.productArray) {
      products = this.state.productArray.map(function(product, index) {
        if(product && product.variations[0].Assets.imagelinks[0]) {
          return (
            <div key={index} className="col-md-4 col-sm-6 col-xs-6 padding_sm category phone ng-scope">
              <Link className="text_3" to={'product/'+product.product_code} >
                <div className="col-xs-12 border none text-center">
                  <div id="myCarousel" className="carousel slide" data-ride="carousel">
                    <div className="carousel-inner" role="listbox">
                      <div className="item active">
                        <img className="first-slide" alt={product.variations[0].Assets.imagelinks[0].alt} src={host+product.variations[0].Assets.imagelinks[0].image} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xs-8 none limit">{product.name}</div>
                <div className="text-right col-xs-4 none limit text_1"><b>{"$"+product.variations[0].Pricing.regularPrice}</b></div>
              </Link>
            </div>   
          );
        }
      })
    }    
    return (
      <div className="">
         <section>
           <div className="col-xs-12 padding_lg">
             <div className="col-xs-12 text-left none f5">
               <span>{products ? ("All Searched Products for '"+ this.state.query+"'.") : ("0 Searched Products for '"+ this.state.query+"'.")}</span>
             </div>
             <div className="col-xs-12 padding_lg">
               {products}
             </div>
           </div>
         </section>
         <div id="scrollDiv">&#160;</div>
      </div>
    )
  }
}

