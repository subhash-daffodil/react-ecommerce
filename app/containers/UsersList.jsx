import React, { Component } from 'react';

export default class UsersList extends Component {
  render() {
    var userList = this.props.users.map((user, i) => {
        return (
            <li key={i}> {user} </li>
        );
    });

    return (
      <div className='users'>
          <h3> Online Users </h3>
          <ul>
              {userList}
          </ul>                
      </div>
    );
  }
}