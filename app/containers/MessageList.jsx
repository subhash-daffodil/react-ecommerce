import React, { Component } from 'react';
import Message from './Message.jsx';

export default class MessageList extends Component {
  render() {
    var messageList = this.props.messages.map((message, i) => {
      return (
        <Message loggedUser = {this.props.loggedUser} key={i} user={message.user} text={message.text} />
      );
    })

    return (
      <div className='messages style-11'>
          <div className="message botMessage">
              <strong>We are here to help you</strong> 
          </div>
          { messageList }
      </div>
    );
  }
}