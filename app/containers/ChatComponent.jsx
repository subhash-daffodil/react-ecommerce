import React, { Component } from 'react';
import UsersList from './UsersList.jsx';
import MessageList from './MessageList.jsx';
import MessageForm from './MessageForm.jsx';
import ChangeNameForm from './ChangeNameForm.jsx';
import config from "../common/config.jsx";
import { signUp, login, setUser, deleteUser, isLoggedIn, me } from '../actions/user.jsx';
// import userStore from '../User/userViewStore.jsx';

export default class ChatComponent extends Component{

  constructor(props) {
    super(props);
    this.state = {
      loggedUser : null,
      users: [],
      messages:[],
      text: '',
      socket1: null
    };
  }

   componentWillMount() {
    var socket = io(config.baseApiUrl);
    this.setState({
      socket1 : socket
    });
   }

  componentDidMount() {
    var that = this;
    var socket = that.state.socket1;
    socket.on('init', function(data){
      var {users, name} = data;
      that.setState({users, user: name});
    });

    socket.on('send:message', function(message){
      var {messages} = that.state;
      messages.push(message);
      that.setState({messages});
      that.scrollDown();
    });

    socket.on('user:join', function(data){
      var {users, messages} = that.state;
      var {name} = data;
      users.push(name);
      // messages.push({
      //     user: 'BOT',
      //     text : name +' Joined'
      // });
      // that.setState({users, messages});
      // that.scrollDown();
    });

    socket.on('user:left', function(data){
      var {users, messages} = that.state;
      var {name} = data;
      var index = users.indexOf(name);
      users.splice(index, 1);
      // messages.push({
      //     user: 'BOT',
      //     text : name +' Left'
      // });
      // that.setState({users, messages});
      // that.scrollDown();
    });

    socket.on('change:name', function(data){
      var {oldName, newName} = data;
      var {users, messages} = that.state;
      var index = users.indexOf(oldName);
      users.splice(index, 1, newName);
      messages.push({
          user: 'BOT',
          text : newName + ' has joined the chat'
      });
      that.setState({users, messages});
    });
  }

  handleMessageSubmit(message) {
    var socket = this.state.socket1;
    var {messages} = this.state;
    // var lastIndex = messages.length - 1;
    // if(messages && messages.length && messages[lastIndex].user === message.user) {
    //   message.text = (<span> {messages[lastIndex].text}  <br/> {message.text}</span>);
    //   messages.pop();
    // }
    messages.push(message);
    this.setState({messages});
    socket.emit('send:message', message);
    this.scrollDown();
  }

  handleChangeName(newName) {
    var socket = this.state.socket1;
    var that = this;
    var oldName = this.state.user;
    socket.emit('change:name', { name : newName}, (result) => {
        if(!result) {
            return console.log('There was an error changing your name');
        }
        var {users} = that.state;
        var index = users.indexOf(oldName);
        users.splice(index, 1, newName);
        that.setState({users, user: newName});
    });
  }

  scrollDown() {
    var height = 0;
    $('.messages .message').each(function(i, value){
        height += parseInt($(this).height());
    });
    height += '';
    $('.messages').animate({scrollTop: height});
  }

  onHeaderClick() {
    var that = this;
    isLoggedIn(function(user) {
      if(user) {
        var userName =  user.username;
        userName = userName.indexOf("@") == -1 ? userName : userName.substring(0, userName.indexOf("@"));
        if(that.state.loggedUser !== userName) {
          that.handleChangeName(userName);
          that.setState({
            loggedUser : userName
          });
          $('#chatComponent').css('bottom', '0px');
        } else {
          if(parseInt($('#chatComponent').css('bottom'))) {
            $('#chatComponent').css('bottom', '0px');
          } else {
            $('#chatComponent').css('bottom', '-265px');
          }
        }
      } else {
        that.props.onOpenCloseDialog(true);
      }
    });
  }

          // <UsersList users={this.state.users}  />
          // <ChangeNameForm onChangeName={(event) => {this.handleChangeName(event)}} />
  
  render() {
    return (
      <div id="chatComponent">
          <div id="chatHeader" onClick={(event) => {this.onHeaderClick(event)}}>
            <h4>Chat Support <span>  <img src="image/chat-support.png" height="20px" width="20px"/></span></h4>
          </div>
          <MessageList loggedUser = {this.state.loggedUser} messages={this.state.messages} />
          <MessageForm onMessageSubmit={(event) => {this.handleMessageSubmit(event)}} user={this.state.user} />
      </div>
    );
  }
}