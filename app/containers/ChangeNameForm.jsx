import React, { Component } from 'react';

export default class ChangeNameForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newName : ''
    };
  }

  onKey(e) {
    this.setState({ newName : e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    var newName = this.state.newName;
    this.props.onChangeName(newName);    
    this.setState({ newName: '' });
  }

  render() {
    return(
      <div className='change_name_form'>
          <h3> Change Name </h3>
          <form onSubmit={(event) => {this.handleSubmit(event)}}>
            <input onChange={(event) => {this.onKey(event)}} value={this.state.newName} />
          </form>  
      </div>
    );
  }
}